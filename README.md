# learned\_group\_changes

Employees transfer to different jobs within an organization. This
script recommends removal of privileges they will no longer need, as
arbitrated by membership in Active Directory groups, by comparing with
properties of all users and groups.

## Input

When someone transfers, people in HR change data in the ERP system. A
list of people whose data was changed is somehow obtained. An email is
automatically sent out containing at least this information:

| Network User | Start Date | Previous Job | New Job | Previous Department | New Department | Name of Manager (OM)  |

## Processing

We gather information about all users and attempt to build a model,
for each group, describing the most important facts about members and
non-members of the group.

For each transferring user x (as laid out in the input), and for each
group of which x is a member, we ask the corresponding model whether
the user x, in their new place in the company, is likely to need
membership in the group.

## Output

At this writing, the recommendations stemming from model predictions,
and explanations, are sent to a log. In future, the recommendations
will be output in a format easy to feed into our future group removal
script.

# Deployment and running

This software needs major Python packages to work, such as NumPy,
scikit-learn, pandas, and matplotlib. The best way to get these
together is with Anaconda. To get it runnable somewhere where you
can't download files, you may need conda-pack.

1. Install Anaconda on a system where you are a normal user.
2. Run an Anaconda command prompt.
3. pip install conda-pack
4. conda env create -f environment.yml
5. conda-pack -n lgcce -o lgcce.zip
6. Get lgcce.zip to the server where the code will run
7. `mkdir x:\learned_group_changes\lgcce` and cd into it.
8. Unzip ...\lgcce.zip
9. Now, from `x:\learned_group_changes`, you can `conda run -p lgcce python -m learned_group_changes`

Look for occurrences of "contoso" to find places where you will likely
need to customize identifiers to fit your own organization.

# Credits

This software contains the dictionary (`/usr/share/dict`) from NetBSD
8.0. See the COPYRIGHT file in `learned_group_changes/netbsd_dict` for
terms.
