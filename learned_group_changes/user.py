import re
import pandas as pd
from numpy import nan
import logging
from learned_group_changes.util import make_short_id
from sklearn.externals.joblib import Memory
from learned_group_changes.abbreviations import unabbreviate

location = './cachedir'
memory = Memory(location, verbose=0)

module_logger = logging.getLogger('l_g_c.u')

class ManagerLoopDetected(Exception):
    pass
class ManagerLoopUnacceptableDegradation(Exception):
    pass

def add_uidx(users):
    # maybe there is a way to not do this
    return users.assign(uidx=users.index)

def add_uidx_mgr01(users):
    return users.assign(**{
        'uidx_mgr01': (users
                     .merge(users,
                            how='left',
                            left_on='Manager',
                            right_on='DistinguishedName')
                      .uidx_y)})


@memory.cache
def add_manager_uidx_tree(users):
    log = module_logger.getChild('add_manager_uidx_tree')
    def manager_column_name(level):
        return 'uidx_mgr{:02d}'.format(level)
    manager_level = 1
    users.loc[:,'manager_problem_at'] = nan
    users_with_k_manager = []
    while True:
        this_manager_column = manager_column_name(manager_level)
        users_k_managing_themselves = users[
            users[this_manager_column] == users.index]
        if not users_k_managing_themselves.empty:
            log.warning('%d users are their own %d-managers. '
                        'Clearing %s attribute for these users. '
                        'See below for list.',
                        len(users_k_managing_themselves), manager_level,
                        this_manager_column)
            users.loc[users[this_manager_column] == users.index,
                      'manager_problem_at'] = manager_level
            users.loc[users[this_manager_column] == users.index,
                      this_manager_column] = nan
        for previous_k in range(1, manager_level):
            pmc = manager_column_name(previous_k)
            users_under_loop = users[users[pmc] == users[this_manager_column]]
            if not users_under_loop.empty:
                log.warning('%d users have the same %d-manager as %d-manager. '
                            'Clearing %s attribute for these users. '
                            'See below for list.',
                            len(users_under_loop), previous_k, manager_level,
                            this_manager_column)
                users.loc[users[pmc] == users[this_manager_column],
                          'manager_problem_at'] = manager_level
                users.loc[users[pmc] == users[this_manager_column],
                          this_manager_column] = nan
        users_with_k_manager.append(users[this_manager_column].count())
        if ((len(users_with_k_manager) >= 2) and
            (users_with_k_manager[-1] == users_with_k_manager[-2])):
            # the number of k-managers has failed to go down as k has
            # increased. this could lead to infinite recursion and is
            # to be avoided
            log.error('%d users have a %d-manager, the same as have a %d-manager!',
                      users_with_k_manager[manager_level-1], manager_level,
                      manager_level-1)
            problem_managers = users.iloc[users[this_manager_column].dropna().unique()]
            raise ManagerLoopDetected(problem_managers.sAMAccountName.to_list())
        if users_with_k_manager[-1] == 0:
            # we have reached the top of the organization without any problems
            break
        log.info('users with a non-NA %s: %d',
                 this_manager_column, users[this_manager_column].count())
        next_manager_column = 'uidx_mgr{:02d}'.format(manager_level+1)
        # for user bob uidx 3, with mgr alice uidx 8, having mgr lupin
        # uidx 10, bob's uidx_mgr01 is 8 as set above; alice's uidx_mgr01
        # is 10 as set above; and with manager_level 1,
        # next_manager_column is 'uidx_mgr02'. bob's uidx_mgr02 is bob's
        # uidx_mgr01's uidx_mgr01, is alice's uidx_mgr01, is 10.
        users = users.assign(**{
            next_manager_column: (users
                                  .merge(users,
                                         how='left',
                                         left_on=this_manager_column,
                                         right_on='uidx')
                                  .loc[:,'uidx_mgr01_y'])})
        manager_level += 1
    if users['manager_problem_at'].count():
        log.warning('(mgr irr) The following users have irregularities '
                    'in their list of n-managers. '
                    'Quality of suggestions for them will be degraded.')
        table = users.loc[users['manager_problem_at'].notna()].sort_values(
            ['manager_problem_at', 'Surname']).to_string(
                columns=['sAMAccountName',
                         'GivenName', 'Surname',
                         'manager_problem_at'],
                formatters={'manager_problem_at': lambda x: str(int(x))})
        indent = 5
        indented = (' ' * indent +
                    table.replace('\n', '\n' + ' ' * indent))
        bar = ' ' * indent + '-' * len(table.split('\n')[-1])
        for line in (indented + '\n' + bar).split('\n'):
            log.warning('(mgr irr) %s', line)
        too_many_proportion = 0.10
        if users['manager_problem_at'].count() > (too_many_proportion * len(users)):
            raise ManagerLoopUnacceptableDegradation(
                'Suggestions for too many users ({} of {}, {:.2f}%) '
                'would be degraded by manager loop problems. '
                '(See log for details on users.) '
                'This exceeds the cutoff {:.2f}%; quitting.'.format(
                    users['manager_problem_at'].count(), len(users),
                    100 * users['manager_problem_at'].count() / len(users),
                    100 * too_many_proportion))
    return users

@memory.cache
def add_nmanager_uidx_feature_columns(users):
    # people have N/A managers, but if we try to make a column name
    # out of that it doesn't compute. let us say that if all feature
    # columns are false, this will denote people who have no
    # (n-)manager. so we lose no statistical data by featurefying.
    all_nmanager_columns = [x for x in users.columns if x.startswith('uidx_mgr')]
    # if any of the manager uws are N/A, the expression won't
    # evaluate. take as an example me, uw yhyyq4u, whose n-managers
    # are 2tgjz6c, g54uu7a, kqf5kmk, j77lstp, N/A, N/A, N/A. the full
    # or-expression with all seven manager levels asking whether
    # our CEO is my n-manager would evaluate to N/A, because some of
    # the values are N/A. the result was that only about 280 people,
    # who were at the deepest org depth and had values for all nmgr
    # fields, got a True for uw_nmgr_p_j77lstp. with fillna, it does
    # the right thing.
    # users[all_nmanager_columns] = users[all_nmanager_columns].fillna()
    all_manager_uidxes = users.uidx_mgr01.dropna().unique()
    def expr_any_nmanager_is(midx):
        return ' or '.join('{} == {!r}'.format(nmc, midx)
                           for nmc in all_nmanager_columns)
    pcols = {'uidx_nmgr_p_{:0.0f}'.format(midx):
             # we need a closure to capture the value of muw at this moment
             (lambda x:
              lambda df: df.eval(expr_any_nmanager_is(x)))(midx)
             for midx in all_manager_uidxes}
    users = users.assign(**pcols)
    return users

@memory.cache
def add_mgr01_uidx_feature_columns(users):
    all_manager_uidxes = users.uidx_mgr01.unique()
    pcols = {'uidx_mgr01_p_{:0.0f}'.format(midx):
             # we need a closure to capture the value of midx at this moment
             (lambda x: lambda df: df.uidx_mgr01 == x)(midx)
             for midx in all_manager_uidxes}
    return users.assign(**pcols)


def massage_titles(users):
    log = module_logger.getChild('massage_titles')
    specials = re.compile(r'\W') # Unicode-expanded equiv of [^A-Za-z0-9_]
    unknowns = set([])
    def log_unknown(word, s):
        unknowns.add(word)
        log.info('unknown word %s (as in title %s); add to '
                 'dictionary or abbreviations', word, s)
    def unabbreviate_and_collect(word):
        return unabbreviate(word, log_unknown)
    users.Title = (users.Title
                   .apply(unabbreviate)
                   .replace(specials,' ')
                   .replace(re.compile(r' +'), ' ')
                   .str.lower()
                   .str.strip()
                   .apply(unabbreviate_and_collect))
    if unknowns:
        log.warning('The following words are unknown in titles. '
                    'Add them to the dictionary or the abbreviation list. '
                    'More details at lower log levels. '
                    'Continuing with reduced data quality.')
        log.warning('Unknown words in titles: %s', ' '.join(unknowns))
    return users

def massage_departments(users):
    log = module_logger.getChild('massage_departments')
    unknowns = set([])
    def log_unknown(word, s):
        unknowns.add(word)
        log.info('unknown word %s (as in dept. %s); add to '
                 'dictionary or abbreviations', word, s)
    def unabbreviate_and_collect(word):
        return unabbreviate(word, log_unknown)
    specials = re.compile(r'\W') # Unicode-expanded equiv of [^A-Za-z0-9_]
    users.Department = (users.Department
                        .apply(unabbreviate)
                        .replace(specials,' ')
                        .replace(re.compile(r' +'), ' ')
                        .str.lower()
                        .str.strip()
                        .apply(unabbreviate_and_collect))
    if unknowns:
        log.warning('The following words are unknown in departments. '
                    'Add them to the dictionary or the abbreviation list. '
                    'More details at lower log levels. '
                    'Continuing with reduced data quality.')
        log.warning('Unknown words in departments: %s', ' '.join(unknowns))
    return users

@memory.cache
def add_title_word_dummies(users):
    # "dummy," also known as "indicator column"
    dummy_data = users.Title.str.get_dummies(sep=' ').add_prefix('t_')
    return users.join(dummy_data)

@memory.cache
def add_department_word_dummies(users):
    # "dummy," also known as "indicator column"
    dummy_data = users.Department.str.get_dummies(sep=' ').add_prefix('d_')
    return users.join(dummy_data)


@memory.cache
def add_group_memberships(users, user_group):
    log = module_logger.getChild('add_group_memberships')
    all_gidxes = user_group.gidx.unique()
    n = len(all_gidxes)
    to_add = pd.DataFrame([])
    for i, gidx in enumerate(all_gidxes):
        to_add['gidx_p_{:0.0f}'.format(gidx)] = users.merge(
            user_group.where(user_group.gidx == gidx),
            on='uidx', how='left').notna().gidx
        if i % (n / 100) == 0:
            log.warning('Adding group columns:   %5d / %5d', i, n)
    return users.join(to_add)

def all_massage_work(users):
    """Add all computed data to the users dataframe.

    At least some of this work will happen in-place. You have been warned.

    """
    # uidx was added earlier. uidx_mgr01 also, so it could be
    # overwritten by replace_changed_positions.
    users = add_manager_uidx_tree(users)
    users = add_nmanager_uidx_feature_columns(users)
    users = add_mgr01_uidx_feature_columns(users)
    users = massage_titles(users)
    users = add_title_word_dummies(users)
    users = massage_departments(users)
    users = add_department_word_dummies(users)
    return users
