from matplotlib.figure import Figure
from subprocess import run, DEVNULL
digest_name = 'sha512' # used to name digests written in quality summary
from hashlib import sha512 as digest
from pickle import dumps
import datetime
from zipfile import ZipFile
import json
from io import TextIOWrapper
import os
import os.path
import csv
from functools import lru_cache
from learned_group_changes.util import make_medium_id

def chunks_of(flo):
    chunk_size = 4096
    while True:
        chunk = flo.read(chunk_size)
        if chunk:
            yield chunk
        else:
            break

def formatted_mtime(fn):
    mtime = os.stat(fn).st_mtime
    dt_mtime = datetime.datetime.fromtimestamp(mtime)
    f_mtime = dt_mtime.isoformat()
    return f_mtime

class NoGitExecutable(Exception):
    pass

class AssessAspect:
    def __init__(self, *args, **kwargs):
        self.input_data_info = []

    def add_input_dependency_filename(self, fn):
        with open(fn, 'rb') as f:
            d = digest()
            for c in chunks_of(f):
                d.update(c)
            self.input_data_info.append((fn, d.hexdigest()))

    @staticmethod
    def git_where_am_i():
        """cwd must be a git repo"""
        try:
            run(['git'], capture_output=True, text=True)
        except FileNotFoundError:
            raise NoGitExecutable()
        # https://git-blame.blogspot.com/2013/06/checking-current-branch-programatically.html
        branch_result = run(['git', 'symbolic-ref', '--short', '-q', 'HEAD', '--'],
                            capture_output=True, text=True)
        branch_result.check_returncode()
        branch = branch_result.stdout.strip()
        # if branch is HEAD, we are at a detached HEAD.
        rev_result = run(['git', 'rev-parse', 'HEAD'], capture_output=True, text=True)
        rev_result.check_returncode()
        rev = rev_result.stdout.strip()
        rev_date_result = run(['git', 'rev-list', 'HEAD', '--max-count', '1',
                               '--date=iso8601', '--format=%ci'],
                              capture_output=True, text=True)
        rev_result.check_returncode()
        rev_date = rev_date_result.stdout.strip()
        # https://stackoverflow.com/a/2659808
        staged_result = run(['git', 'diff-index', '--quiet', '--cached', 'HEAD', '--'])
        if staged_result.returncode == 0:
            staged = False
        elif staged_result.returncode == 1:
            staged = True
        else:
            staged_result.check_returncode()
        changes_result = run(['git', 'diff-files', '--quiet'])
        if changes_result.returncode == 0:
            changes = False
        elif changes_result.returncode == 1:
            changes = True
        else:
            changes_result.check_returncode()
        untracked_result = run(['git', 'ls-files', '--others', '--exclude-standard'],
                               capture_output=True, text=True)
        untracked_result.check_returncode()
        untracked = [x.strip() for x in untracked_result.stdout.split('\n')
                     if x.strip()]
        return {
            'branch': branch,
            'rev': rev,
            'rev_date': rev_date,
            'working_changes': changes,
            'staged_changes': staged,
            'untracked_unignored_files': untracked,
        }

    @property
    @lru_cache()
    def run_now(self):
        return datetime.datetime.now(datetime.timezone.utc)
    
    @property
    @lru_cache()
    def run_id(self):
        checksum = make_medium_id(dumps(self.input_data_info))
        now = self.run_now
        date = now.strftime('%Y-%m-%dT%H%M%S%z')
        return f'lgc_{date}_{checksum}'

    def create_accuracy_report(self):
        log = self.log.getChild('create_accuracy_report')
        name = '{}_quality.zip'.format(self.run_id)
        with ZipFile(name, 'w') as z:
            with z.open('inputs.csv', 'w') as inp:
                cwd = os.getcwd()
                with TextIOWrapper(inp, newline='') as tio:
                    c = csv.writer(tio)
                    c.writerow(('file', 'mtime', digest_name))
                    for fn, d in self.input_data_info:
                        c.writerow((os.path.join(cwd, fn),
                                    formatted_mtime(fn),
                                    d))
            with z.open('accuracy_scores.csv', 'w') as acc:
                with TextIOWrapper(acc) as tio:
                    self.scores.to_csv(tio)
            with z.open('accuracy_histogram.png', 'w') as png:
                fig = Figure(figsize=(4,3), dpi=120)
                ax = fig.add_subplot(1,1,1)
                ax.hist(self.scores.accuracy)
                n = len(self.scores)
                ax.set_title(f'Histogram of model accuracy (F1, n={n})')
                fig.tight_layout()
                fig.savefig(png, dpi='figure')
            with z.open('summary.json.txt', 'w') as summary:
                with TextIOWrapper(summary) as tio:
                    zero = self.scores.accuracy[self.scores.accuracy == 0]
                    nonzero = self.scores.accuracy[self.scores.accuracy != 0]
                    json.dump({
                        'n': int(self.scores.accuracy.count()),
                        'n_fit': int(nonzero.count()),
                        'n_unfit': int(zero.count()),
                        'fit_accuracy_quartiles': [
                            nonzero.quantile(x)
                        for x in [0.0, 0.25, 0.5, 0.75, 1.0]],
                    }, tio, indent=4)
            if os.path.exists('.git'):
                with z.open('code.json.txt', 'w') as code:
                    with TextIOWrapper(code) as tio:
                        try:
                            json.dump(self.git_where_am_i(), tio, indent=4)
                        except NoGitExecutable:
                            json.dump({}, tio, indent=4)
        log.info('saved report %s', name)
