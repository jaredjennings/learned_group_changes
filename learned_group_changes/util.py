from hashlib import sha3_512 as digest
from base64 import b32encode

def make_short_id(s):
    """Make a short identifier for a thing among 64k other things.
    
    This hashes the thing and makes an identifier from at least 32
    bits of the hash.

    """
    return (b32encode(digest(repr(s).encode('utf-8')).digest()).decode('utf-8'))[:7].lower()

def make_medium_id(*args):
    """Make an identifier for a thing among 2^32 other things.
    
    This hashes the thing and makes an identifier from at least 48
    bits of the hash.

    """
    return (b32encode(digest(b'\0'.join(repr(x).encode('utf-8') for x in args)).digest()).decode('utf-8'))[:10].lower()
