# https://stackoverflow.com/a/20885799
import importlib.resources as res
import learned_group_changes.netbsd_dict as nd

words = set()

for package in (nd,):
    for fn in res.contents(package):
        if fn[0].isupper():
            continue
        if fn[0] == '_':
            continue
        if res.is_resource(package, fn):
            for line in res.read_text(package, fn).split('\n'):
                words.add(line)

def is_word(s):
    return s in words
