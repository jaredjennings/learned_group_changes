# https://stackoverflow.com/a/20885799
import importlib.resources as res
import learned_group_changes.netbsd_dict
import learned_group_changes.other_dictionaries
import learned_group_changes.abbreviation_lists

_words = set()

for package in (learned_group_changes.netbsd_dict,
                learned_group_changes.other_dictionaries):
    for fn in res.contents(package):
        if fn[0].isupper():
            continue
        if fn[0] == '_':
            continue
        if res.is_resource(package, fn):
            for line in res.read_text(package, fn).split('\n'):
                _words.add(line.lower())

_abbrevs = {}

for package in (learned_group_changes.abbreviation_lists,):
    for fn in res.contents(package):
        if fn[0].isupper():
            continue
        if fn[0] == '_':
            continue
        if res.is_resource(package, fn):
            try:
                for i, line in enumerate(res.read_text(package, fn).split('\n')):
                    if line and not line.startswith('#'):
                        try:
                            abbr, exp = [x.strip().lower() for x in line.split(':')]
                            _abbrevs[abbr] = exp
                        except Exception as e:
                            e.args = ('at line', i) + e.args
                            raise
            except Exception as e:
                e.args = ('in', package, fn) + e.args
                raise

def is_word(s):
    '''Find whether a string is a word.

    This is not case-sensitive, and not smart at all. It just finds
    out whether the string is in a set of words. Its use is to try to
    distinguish abbreviations from normal words.

    '''
    return s.lower() in _words

def do_nothing(*args, **kwargs):
    '''Do nothing. Return None.'''
    pass

def unabbreviate(s, unknown=do_nothing):
    '''Expand known abbreviations in s (and convert to lowercase).

    If an unknown word is encountered, it is left alone, and unknown
    is called with the unknown word and the string it came in as
    arguments. unknown's return value is ignored. For example:

    >>> def unk(word, phrase): print('eyyy unknown', word, 'as in', phrase)
    >>> r = unabbreviate('Mtce Pipe Fnord', unk)
    eyyy unknown fnord as in Mtce Pipe Fnord
    >>> r
    maintenance pipe fnord

    The setting of this function is that we are improving data where
    possible. If it is not possible to improve, we move forward with
    the data we have; that is not an exception. Making things better
    next time will involve collecting unknown words and acting to make
    them known, one way or another. That is outside the remit of this
    function, and likely outside this run of the script. That's why we
    have an unknown callable, instead of raising an exception or
    logging unknown words at this level.

    '''
    if not isinstance(s, str): # nan, for example
        return s
    un_words = []
    for utterance in s.lower().split():
        if utterance in _abbrevs:
            un_words.append(_abbrevs[utterance])
        elif is_word(utterance):
            un_words.append(utterance)
        else:
            un_words.append(utterance)
            unknown(utterance, s)
    return ' '.join(un_words)
