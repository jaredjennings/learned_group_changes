"""Code to explain why our model predicted what it did.

"""
import pandas as pd
from enum import Enum

class Sn(Enum):
    """Features of parts of sentences (whence Sn).

    The parts of sentences labelled by values here will be put
    together into explanations of what our models found to be the most
    important attributes distinguishing members and non-members of a
    group.

    """
    SO = 1 # positive
    NOT = 2 # negative
    PL = 3 # plural
    SING = 4 # singular

class ExplainAspect:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def uidx2name(self, uidx):
        """Find the name of a user having a given index in the users table."""
        try:
            uidx = int(uidx)
            gn = self.users.iloc[uidx].GivenName
            sn = self.users.iloc[uidx].Surname
            sam = self.users.iloc[uidx].sAMAccountName
            return f'{gn} {sn} ({sam})'
        except IndexError:
            return f'(unknown user with uidx {uidx!r})'
        except ValueError:
            return f'(invalid uidx {uidx!r})'

    def uidx2sid(self, uidx):
        """Find the SID of a user having a given index in the users table."""
        try:
            uidx = int(uidx)
            return self.users.iloc[uidx].SID
        except IndexError:
            return f'(unknown user with uidx {uidx!r})'
        except ValueError:
            return f'(invalid uidx {uidx!r})'

    def uidx2sAMAccountName(self, uidx):
        """Find the sAMAccountName of a user having a given index in the users table."""
        uidx = int(uidx)
        # if anything goes wrong, we want to fail
        return self.users.iloc[uidx].sAMAccountName

    def gidx2name(self, gidx):
        """Find the name of a group having a given index in the groups table."""
        try:
            return self.groups.iloc[gidx].Name
        except TypeError:
            # maybe gidx was not a number but '' or something
            return f'(unknown group with gidx {gidx!r})'

    def gidx2sid(self, gidx):
        """Find the SID of a group having a given index in the groups table."""
        try:
            return self.groups.iloc[gidx].SID
        except TypeError:
            # maybe gidx was not a number but '' or something
            return f'(unknown group with gidx {gidx!r})'

    def interpret_independent_column_names(self, weights):
        meanings = {
            't_':
            lambda w, t: 'Title {} word {}'.format(
                'contains', t),
            'uidx_mgr01_p_':
            lambda w, m: 'Direct manager {} {}'.format(
                'is', self.uidx2name(m)),
            'uidx_nmgr_p_':
            lambda w, m: 'n-manager {} {}'.format(
                'is', self.uidx2name(m))
        }
        def interpret_one_weight_name(weighttuple):
            for prefix, clbl in meanings.items():
                if weighttuple.feature.startswith(prefix):
                    remainder = weighttuple.feature[len(prefix):]
                    return clbl(weighttuple.weight, remainder)
            return '({})'.format(weighttuple.feature)
        return [interpret_one_weight_name(t) for t in weights.itertuples()]

    column_clauses = {
        't_': {
            frozenset((Sn.PL, Sn.SO)): 'have word {} in Title',
            frozenset((Sn.PL, Sn.NOT)): 'do not have word {} in Title',
            frozenset((Sn.SING, Sn.SO)): 'has word {} in Title',
            frozenset((Sn.SING, Sn.NOT)): 'does not have word {} in Title',
        },
        'd_': {
            frozenset((Sn.PL, Sn.SO)): 'have word {} in Department',
            frozenset((Sn.PL, Sn.NOT)): 'do not have word {} in Department',
            frozenset((Sn.SING, Sn.SO)): 'has word {} in Department',
            frozenset((Sn.SING, Sn.NOT)): 'does not have word {} in Department',
        },
        'uw_mgr01_p_': {
            frozenset((Sn.PL, Sn.SO)): 'have direct manager {}',
            frozenset((Sn.PL, Sn.NOT)): 'do not have direct manager {}',
            frozenset((Sn.SING, Sn.SO)): 'has direct manager {}',
            frozenset((Sn.SING, Sn.NOT)): 'does not have direct manager {}',
        },
        'uidx_mgr01_p_': {
            frozenset((Sn.PL, Sn.SO)): 'have direct manager {}',
            frozenset((Sn.PL, Sn.NOT)): 'do not have direct manager {}',
            frozenset((Sn.SING, Sn.SO)): 'has direct manager {}',
            frozenset((Sn.SING, Sn.NOT)): 'does not have direct manager {}',
        },
        'uw_nmgr_p_': {
            frozenset((Sn.PL, Sn.SO)): 'have n-manager {}',
            frozenset((Sn.PL, Sn.NOT)): 'do not have n-manager {}',
            frozenset((Sn.SING, Sn.SO)): 'has n-manager {}',
            frozenset((Sn.SING, Sn.NOT)): 'does not have n-manager {}',
        },
        'uidx_nmgr_p_': {
            frozenset((Sn.PL, Sn.SO)): 'have n-manager {}',
            frozenset((Sn.PL, Sn.NOT)): 'do not have n-manager {}',
            frozenset((Sn.SING, Sn.SO)): 'has n-manager {}',
            frozenset((Sn.SING, Sn.NOT)): 'does not have n-manager {}',
        },
    }

    def get_all_independent_column_name_explanations(self):
        column_transforms = {
            't_': lambda t: t,
            'd_': lambda d: d,
            'uidx_mgr01_p_': self.uidx2name,
            'uidx_nmgr_p_': self.uidx2name,
        }
        log = self.log.getChild('g_a_i_c_n_e')
        log.debug('begin')
        column_explanations = {}
        for col in self.independent_columns:
            for prefix, clauses in self.column_clauses.items():
                if col.startswith(prefix):
                    remainder = col[len(prefix):]
                    particle = column_transforms[prefix](remainder)
                    for clause_kind, format_string in clauses.items():
                        column_explanations[col,clause_kind] = (
                            format_string.format(particle))
        log.debug('end')
        return column_explanations


# one of these is chosen in ad_data.py

class LogisticRegressionExplainAspect:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_model_weights(self, gidxp):
        model = self.models[gidxp]
        weights = pd.DataFrame(data=model.coef_,
                               columns=self.independent_columns).melt()
        weights.columns = ['feature', 'weight']
        weights = weights.assign(absweight=weights.loc[:, 'weight'].abs())
        return weights

    def explain_salient_user_features(self, gidxp, moving_user):
        weights = self.get_model_weights(gidxp)
        X = moving_user[self.independent_columns].copy().values
        X[X==0] = False
        X[X==1] = True
        w_here = weights.assign(X=X)
        clauses = [self.column_explanations[t.feature,
                                            frozenset((Sn.SING,
                                                       Sn.SO if t.X else Sn.NOT))]
                   for t in w_here.itertuples()]
        gm = [('also do' if t.X else 'do') if (t.weight > 0) else
              ("don't" if t.X else "also don't")
              for t in w_here.itertuples()]
        return weights.assign(moving_user=clauses,
                              group_members=gm)

    def why_kick_user_out(self, gidxp, moving_user):
        weights_here = self.explain_salient_user_features(gidxp, moving_user)
        top5 = weights_here.sort_values(['absweight'],
                                        ascending=False).head(5)
        table = top5.to_string(columns=['moving_user',
                                        'group_members',
                                        'absweight'])
        indent = 5
        indented = (' ' * indent +
                    table.replace('\n', '\n' + ' ' * indent))
        bar = ' ' * indent + '-' * len(table.split('\n')[-1])
        return indented + '\n' + bar


class RandomForestsExplainAspect:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_feature_importances(self, gidxp):
        model = self.models[gidxp]
        impo = pd.DataFrame(data=model.feature_importances_.reshape(1,-1),
                            columns=self.independent_columns).melt()
        impo.columns = ['feature', 'importance']
        return impo

    def explain_salient_user_features(self, gidxp, moving_user):
        impo = self.get_feature_importances(gidxp)
        X = moving_user[self.independent_columns].copy().values
        X[X==0] = False
        X[X==1] = True
        im_here = impo.assign(X=X)
        clauses = [self.column_explanations[t.feature,
                                            frozenset((Sn.SING,
                                                       Sn.SO if t.X else Sn.NOT))]
                   for t in im_here.itertuples()]
        gm = ['?'
              # ('also do' if t.X else 'do') if (t.weight > 0) else
              # ("don't" if t.X else "also don't")
              for t in im_here.itertuples()]
        return impo.assign(moving_user=clauses,
                           group_members=gm)


    def why_act_on_user(self, gidxp, moving_user):
        impo_here = self.explain_salient_user_features(gidxp, moving_user)
        top5 = impo_here.sort_values(['importance'],
                                     ascending=False).head(5)
        table = top5.to_string(columns=['moving_user',
                                        'group_members',
                                        'importance'])
        indent = 5
        indented = (' ' * indent +
                    table.replace('\n', '\n' + ' ' * indent))
        bar = ' ' * indent + '-' * len(table.split('\n')[-1])
        return indented + '\n' + bar
