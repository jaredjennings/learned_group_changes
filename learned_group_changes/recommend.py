# -*- coding: utf-8-dos; -*-
from logging import getLogger
from learned_group_changes.logging import item_of_business
from learned_group_changes.ad_data import ActiveDirectoryData
from dataclasses import dataclass
from datetime import datetime, timedelta

@dataclass
class Change:
    group: str
    action: str
    sAMAccountName: str
    confirmation: str
    description: str
    date: datetime
    accuracy: float
    UserSID: str
    GroupSID: str

def recommend_group_changes(d: ActiveDirectoryData):
    """Use models to recommend removing transferring users from some groups.

    """
    log = getLogger('r_g_r')
    # a week from now, to give time for review.
    when = d.run_now + timedelta(7,0,0)
    gidxps = [col for col in d.users.columns
              if col.startswith('gidx_p')]
    recommendation_bar = d.scores.accuracy[d.scores.accuracy != 0].quantile(0.75)
    # https://stackoverflow.com/q/15943769
    log.debug('there are %d users to recommend group changes for',
              d.users[d.users.reality_now == False].shape[0])
    for index, moving_user in (d.users[
            d.users.reality_now == False]).iterrows():
        normal_name = d.uidx2name(index)
        user_sid = d.uidx2sid(index)
        user_sam = d.uidx2sAMAccountName(index)
        log.debug('recommending for %s', user_sam)
        for gidxp in gidxps:
            model = d.models[gidxp]
            accuracy = d.scores.loc[d.scores.gidxp == gidxp, 'accuracy'].iat[0]
            plusminus = d.scores.loc[d.scores.gidxp == gidxp, 'plusminus'].iat[0]
            gidx = int( gidxp[len('gidx_p_'):] )
            group_name = d.gidx2name(gidx)
            group_sid = d.gidx2sid(gidx)
            with item_of_business(d.run_id, user_sid, group_sid) as iob:
                is_ = moving_user[gidxp]
                if accuracy > 0.5:
                    should = model.predict(moving_user[d.independent_columns].values.reshape(1,-1))
                    do = None
                    if not is_ and should:
                        do = 'add'
                        # adding requires approvals; never recommend
                        # nor automatically do it
                        counsel_format = 'consider adding {} to'
                        character = '?'
                    # here is where we would suggest groups to add the
                    # user to, if we did that sort of thing. But who are
                    # we, the Cyberusability Team? ;)
                    if is_ and not should:
                        do = 'remove'
                        if accuracy > recommendation_bar:
                            counsel_format = 'remove {} from'
                            character = 'y'
                        else:
                            counsel_format = 'consider removing {} from'
                            character = '?'
                    if do:
                        log.info('%s group %s (model accuracy %0.2f±%0.2f)',
                                 counsel_format.format(normal_name), group_name,
                                 accuracy, plusminus)
                        for line in d.why_act_on_user(gidxp, moving_user).split('\n'):
                            log.info('why: %s', line)
                        pcrd = d.position_change_raw_data[user_sam]
                        old_position = pcrd['Previous Job']
                        new_position = pcrd['New Job']
                        log.debug('yielding a Change')
                        yield Change(
                            # date was pcrd['Start Date'], but
                            # effective dates for position changes are
                            # possibly not close to now in every case.
                            # the idea of the date column for a group
                            # change is not to utter a fact about the
                            # position change, but to do timely
                            # changes while allowing for review.
                            date=when,
                            sAMAccountName='SAM: {}'.format(user_sam),
                            UserSID=user_sid,
                            group=group_name,
                            action=do,
                            GroupSID=group_sid,
                            description=f'position change {iob}: {normal_name}, {old_position} -> {new_position}',
                            accuracy=accuracy,
                            confirmation=character)
                        log.debug('yielded a Change')
                else:
                    if is_:
                        log.info('could not determine whether %s should be '
                                 'removed from group %s (model accuracy %0.2f±%0.2f)',
                                 normal_name, group_name, accuracy, plusminus)
