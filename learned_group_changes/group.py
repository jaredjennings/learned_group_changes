import re
import pandas as pd
import logging
from learned_group_changes.util import make_short_id

def add_gw(groups):
    return groups.assign(gw=lambda df: [make_short_id(sid) for sid in df.SID])

def add_gidx(groups):
    return groups.assign(gidx=groups.index)

def all_massage_work(groups):
    """Do everything to improve groups."""
    return add_gidx(groups)
