import sys
import logging
import logging.config
from contextlib import contextmanager
from .util import make_medium_id

current_item_of_business = '-'

def set_iob(run_id, user_sid, group_sid):
    global current_item_of_business
    current_item_of_business = 'iob' + make_medium_id(run_id, user_sid, group_sid)

def no_iob():
    global current_item_of_business
    current_item_of_business = '-'

@contextmanager
def item_of_business(run_id, user_sid, group_sid):
    set_iob(run_id, user_sid, group_sid)
    yield current_item_of_business
    no_iob()

class ItemOfBusinessRecord(logging.LogRecord):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.iob = current_item_of_business

def getLogger(*args, **kwargs):
    return logging.getLogger(*args, **kwargs)

@contextmanager
def logging_handled(loggingConfig):
    # "1 to select line buffering"
    with open('out.txt', 'wt', buffering=1) as out:
        logging.setLogRecordFactory(ItemOfBusinessRecord)
        logging.config.dictConfig(loggingConfig)
        try:
            yield
        finally:
            logging.shutdown()
