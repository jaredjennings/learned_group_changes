from logging import getLogger
from .logging import logging_handled
import sys
from .ad_data import ActiveDirectoryData
from .recommend import recommend_group_changes, Change
from dataclasses import fields, asdict
from contextlib import contextmanager
import csv
import argparse
import yaml

p = argparse.ArgumentParser(
    description='Machine-learn changes to the AD group memberships '
    'of users who are transferring inside the organization.')
p.add_argument('-c', '--config',
               type=argparse.FileType('r', encoding='UTF-8'),
               help='the configuration TOML file',
               required=True)
p.add_argument('--users-csv',
               help='the ad-user.csv file output by dump-ad-info.ps1',
               required=True)
p.add_argument('--groups-csv',
               help='the ad-group.csv file output by dump-ad-info.ps1',
               required=True)
p.add_argument('--users-groups-csv',
               help='the ad-user-group.csv file output by dump-ad-info.ps1',
               required=True)
p.add_argument('--position-change-csv',
               help='the list of position changes',
               required=True)
p.add_argument('-o', '--output',
               help='the file to which to write group change recommendations',
               required=True)
args = p.parse_args()

c = yaml.safe_load(args.config)

with logging_handled(c['logging']):
    log = getLogger('main')
    with open(args.position_change_csv) as poschg_csv_data:
        try:
            d = ActiveDirectoryData(poschg_csv_filename=args.position_change_csv,
                                    poschg_csv_data=poschg_csv_data,
                                    users_csv_filename=args.users_csv,
                                    groups_csv_filename=args.groups_csv,
                                    users_groups_csv_filename=args.users_groups_csv)
            with open(args.output, 'wt', encoding='UTF-8', newline='') as rec_out:
                w = csv.DictWriter(rec_out,
                                   fieldnames=[f.name for f in fields(Change)])
                w.writeheader()
                for chg in recommend_group_changes(d):
                    w.writerow(asdict(chg))
                    log.debug('wrote a Change')
            d.create_accuracy_report()
        except Exception as e:
            log.exception('quitting due to uncaught exception')
            sys.exit(2)
