import re
import pandas as pd
import logging
from learned_group_changes.util import make_short_id

def add_uidx(user_group, users):
    return user_group.assign(
        uidx=(user_group.merge(users,
                               how='left',
                               left_on='SID',
                               right_on='SID')
              .uidx))

def add_gidx(user_group, groups):
    return user_group.assign(
        gidx=(user_group.merge(groups,
                               how='left',
                               left_on='MemberOfGroupWithDN',
                               right_on='DistinguishedName')
              .gidx))


def all_massage_work(user_group, users, groups):
    user_group = add_uidx(user_group, users)
    user_group = add_gidx(user_group, groups)
    return user_group
