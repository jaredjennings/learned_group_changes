from logging import getLogger
from learned_group_changes.obtain import ObtainAspect
from learned_group_changes.explain import (
    ExplainAspect, RandomForestsExplainAspect, LogisticRegressionExplainAspect)
from learned_group_changes.calculate import (
    CalculateAspect, logistic_regression_factory, random_forests_factory)
from learned_group_changes.assess import AssessAspect
from learned_group_changes.user import add_uidx, add_uidx_mgr01

# model choosing, step 1: subclass the appropriate explain aspect
# class, in addition to ExplainAspect.
class ActiveDirectoryData(ObtainAspect, ExplainAspect, RandomForestsExplainAspect,
                          CalculateAspect, AssessAspect):
    """The container for what would otherwise be a bunch of global variables.

    """

    # model choosing, step 2: set model_factory to the appropriate
    # factory function.
    model_factory = staticmethod(random_forests_factory)

    def __init__(self, *args, poschg_csv_filename, poschg_csv_data,
                 users_csv_filename, groups_csv_filename,
                 users_groups_csv_filename, **kwargs):
        super().__init__(*args, **kwargs)
        self.log = getLogger('ActDirData')
        self.obtain_dataframes(users_csv_filename, groups_csv_filename,
                               users_groups_csv_filename)
        self.users = add_uidx(self.users)
        # add the manager index so that replace_changed_positions can
        # overwrite it
        self.users = add_uidx_mgr01(self.users)
        self.replace_changed_positions(poschg_csv_filename, poschg_csv_data)
        self.massage()
        self.fit_models()
