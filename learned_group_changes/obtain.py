"""Code to obtain data: about position changes and about AD objects."""

import pandas as pd
import csv
import learned_group_changes.user
import learned_group_changes.group
import learned_group_changes.user_group
import datetime

class ObtainAspect:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def obtain_dataframes(self, users_csv_fn, groups_csv_fn,
                          users_groups_csv_fn):
        """Get the Active Directory data from the dumped csv files."""
        log = self.log.getChild('o_df_f_s')
        log.info('Obtaining dataframes from CSVs.')
        self.users = pd.read_csv(users_csv_fn)
        self.users['reality_now'] = True
        self.groups = pd.read_csv(groups_csv_fn)
        self.user_group = pd.read_csv(users_groups_csv_fn)
        self.add_input_dependency_filename(users_csv_fn)
        self.add_input_dependency_filename(groups_csv_fn)
        self.add_input_dependency_filename(users_groups_csv_fn)

    def massage(self):
        """Turn AD data into features suitable for input to model fitting."""
        log = self.log.getChild('massage')
        log.info('adding data columns to users')
        self.users = learned_group_changes.user.all_massage_work(self.users)
        log.info('adding data columns to groups')
        self.groups = learned_group_changes.group.all_massage_work(self.groups)
        log.info('adding data columns to user_group')
        self.user_group = learned_group_changes.user_group.all_massage_work(
            self.user_group, self.users, self.groups)
        log.info('adding group membership columns to users')
        self.users = learned_group_changes.user.add_group_memberships(
            self.users, self.user_group)
        # this is a thing about making models, but we need it to exist
        # even if the fit models are cached
        self.independent_columns = [c for c in self.users.columns
                                    if 'mgr01_p' in c
                                    or 'nmgr_p' in c
                                    or c.startswith('t_')
                                    or c.startswith('d_')]
        self.column_explanations = self.get_all_independent_column_name_explanations()

    def replace_changed_positions(self, poschg_csv_filename, poschg_csv_data, now=None):
        """Replace AD-observed data about moving users with position-change data."""
        # the position-changed user data has to be massaged, so to add
        # all the uw_mgr1_p and uw_nmgr_p columns. but furthermore,
        # the position-changed user data has to be in with the
        # existing AD data because there is a possibility however
        # slight that the new job title has a word that doesn't occur
        # in anyone else's job title, and if all the users don't have
        # a column for that word, the position-changed data and the
        # other data will not be of the same shape. we will just have
        # to exclude the new data from training and testing.
        log = self.log.getChild('a_c_p')
        changed = 0
        self.position_change_raw_data = {}
        if now is None:
            now = datetime.datetime.now()
        for row in csv.DictReader(poschg_csv_data):
            row['NetworkUser'] = sAM = row['Network User'].lower()
            if row['Previous Department'] == row['New Department']:
                self.log.info('%s is changing positions within the same '
                              'department (e.g. promotion); skipping', sAM)
                continue
            if ((row['New Job'].strip() == '') or
                (row['New Department'].strip() == ''):
                # avoid removing privileges from someone because of
                # insufficient data
                self.log.info('%s New Job and/or New Department is empty; '
                              'skipping', sAM)
                continue
            row['Start Date'] = datetime.datetime.strptime(row['Start Date'],
                                                           '%m/%d/%Y')
            if (now - row['Start Date']) > timedelta(365,0,0):
                self.log.info('%s Start Date for New Job %s '
                              '(%s) is more than a year ago; skipping',
                              sAM, row['New Job'], row['Start Date'])
                continue
            try:
                new_manager_name = row['Name of Manager (OM)']
                new_manager_uidx = self.firstlast2uidx(new_manager_name)
            except KeyError as e:
                self.log.error('%s new manager is %s, but %r; skipping',
                               sAM, new_manager_name, e)
                continue
            # if there is existing data about the user (two position
            # changes) then we will overwrite it
            self.position_change_raw_data[sAM] = row.copy()
            self.log.debug('%s is changing positions', sAM)
            assert len(self.users[self.users.sAMAccountName.str.lower() == sAM]) == 1, 'multiple users with sAMAccountName {}'.format(sAM)
            self.users.loc[self.users.sAMAccountName.str.lower() == sAM,
                           'reality_now'] = False
            self.users.loc[self.users.sAMAccountName.str.lower() == sAM,
                           'uidx_mgr01'] = new_manager_uidx
            self.users.loc[self.users.sAMAccountName.str.lower() == sAM,
                           'Department'] = row['New Department']
            self.users.loc[self.users.sAMAccountName.str.lower() == sAM,
                           'Title'] = row['New Job']
            changed += 1
        log.info('changed %d users', changed)
        self.add_input_dependency_filename(poschg_csv_filename)
