# -*- coding: utf-8-dos; -*-
"""This is where the modelling happens.

"""
from logging import getLogger
from numpy import nan
import pandas as pd
import warnings
from sklearn.exceptions import (
    ConvergenceWarning, FitFailedWarning, UndefinedMetricWarning)
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, StratifiedKFold
from sklearn.externals.joblib import Memory

location = './models-cachedir'
memory = Memory(location, verbose=0)

# see ad_data.py for the choice among these two
def logistic_regression_factory():
    return LogisticRegression(
        penalty='l1', class_weight='balanced',
        solver='liblinear', max_iter=1000)

def random_forests_factory():
    return RandomForestClassifier(
        max_features=15, n_estimators=100)

module_logger = getLogger('l_g_c.calc')

@memory.cache
def fit_models_outside_object(u, g, independent_columns, model_factory):
    """Try to characterize members of groups using independent_columns.

    This is a function that takes two big tables, rather than a method
    of ActiveDirectoryData, so that the inputs will be evident to
    joblib.Memory and it will cache our work.

    """
    log = module_logger.getChild('f_m_o_o')
    folds = 3
    X = u[independent_columns]
    models = {}
    scores_rows = []
    all_gidxps = u.columns[u.columns.str.startswith('gidx_p_')]
    with warnings.catch_warnings():
        warnings.filterwarnings('error')
        # UndefinedMetricWarning: "F-score is ill-defined and
        # being set to 0.0 in labels with no predicted samples."
        # In other words, with this test fold, our model predicted
        # either all True, user should be in the group, or (more
        # likely I think) all False, despite our efforts (by using
        # a Stratified* validation iterator) to include in our
        # training and testing set vectors where the label is both
        # True and False.
        #
        # An F-score of 0 is a sufficient response: we will be
        # filtering our models by their scores and this should
        # cause such a model to be rejected as unfit (heh) to
        # model whether people should be in its corresponding AD
        # group.
        warnings.filterwarnings('ignore', category=UndefinedMetricWarning)
        total = len(all_gidxps)
        for i, gidxp in enumerate(list(all_gidxps)):
            gidx = int(gidxp[len('gidx_p_'):])
            accuracy = 0.0
            plusminus = 0.0
            group_name = g.iloc[gidx].Name
            n = len(u[u[gidxp]])
            if i % 50 == 0:
                log.warning('Fitting and scoring models:  %5d / %5d', i, total)
            models[gidxp] = model_factory()
            try:
                if n > folds:
                    y = u.loc[:, gidxp]
                    cv = StratifiedKFold(n_splits=folds)
                    scores = cross_val_score(models[gidxp], X, y, cv=cv,
                                             scoring='f1_macro',error_score=nan)
                    accuracy, plusminus = (scores.mean(), scores.std()*2)
                    models[gidxp].fit(X, y)
                    log.debug('%5d/%5d n=%5d F1=%0.2f±%0.2f - %s',
                              i, total, n, accuracy, plusminus, group_name)
                else:
                    log.debug('not enough users (n=%d < min %d) '
                              'in group %r to learn who should '
                              'be in the group; scoring 0',
                              n, folds, group_name)
                    accuracy, plusminus = 0.0, 0.0
            except (ConvergenceWarning, FitFailedWarning):
                log.info('could not fit an estimator for group %r '
                         '(%s), n=%d; scoring 0', group_name, gidxp, n)
                accuracy, plusminus = 0.0, 0.0
            except Exception as e:
                e.args = ('while fitting model for group', group_name) + e.args
                raise
            scores_rows.append((gidxp, accuracy, plusminus, n))
        log.info('model fitting done')
        scores = pd.DataFrame(scores_rows, columns=(
            'gidxp', 'accuracy', 'plusminus', 'n'))
        return models, scores


class CalculateAspect:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def firstlast2uidx(self, firstlast):
        """Find the index of a user from a first and last name.

        The new manager of a moving user is listed in our input as a
        first and last name.

        """
        pieces = firstlast.split()
        first, last = None, None
        if len(pieces) < 2:
            raise KeyError("don't know how to look up name", firstlast)
        elif len(pieces) == 2:
            first, last = pieces
        else:
            # assume middle name. maybe not the best assumption. let's
            # go with it till it breaks
            first = pieces[0]
            last = pieces[-1]
        matches = self.users[(self.users.GivenName.str.startswith(first))
                             &
                             (self.users.Surname.str.startswith(last))]
        if len(matches) == 1:
            return matches.index[0]
        else:
            raise KeyError('for firstlast', firstlast, 'found',
                           len(matches), 'matches, not 1')


    def fit_models(self):
        """Do the calculations to make a model for membership in each group."""
        log = self.log.getChild('fit_models')
        folds = 3
        u = self.users[self.users.reality_now == True]
        self.log.info('fitting models')
        self.models, self.scores = fit_models_outside_object(
            u, self.groups, self.independent_columns, self.model_factory)
