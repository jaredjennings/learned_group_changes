#+TITLE: Removing access when employees transfer, using machine learning
#+AUTHOR: Jared Jennings
#+DATE: [2020-03-02 Mon]
#+STARTUP: beamer
#+STARTUP: oddeven
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+BEAMER_THEME: Pittsburgh
#+OPTIONS: H:2 toc:t
#+EXCLUDE_TAGS: noexport

# for a column view of options and configurations for the individual
# frames
#+COLUMNS: %20ITEM %13BEAMER_env(Env) %6BEAMER_envargs(Args) %4BEAMER_col(Col) %7BEAMER_extra(Extra)

* Description                                                      :noexport:
** Short
In a world where users are granted access to apps and shares using
Active Directory groups (but don't know what they really need and keep
transferring around the company), one guy is just trying to make sense
of it all. Follow his efforts to remove unneeded access with the power
of math!
** Long
The principle of least privilege demands that if someone doesn't need
some access anymore to do their job, the access should be removed. We
know when people need more access, because they request it. But no one
asks for less access.

With hundreds to thousands of Active Directory groups, some stretching
back 15 years across three mergers, it's not always clear what access
a group confers, who should be in it - and who shouldn't anymore.

This is the story of how we started to use machine learning to look at
the access of the people around someone who is transferring inside our
organization, and suggest what access the transferring person might no
longer need.

* Problem
** Hi!
- Jared
- SOX
- Slides available - URL at end
** Active Directory (AD)
- Users authenticated by AD
- Users authorized with AD groups, where possible
  - Applications
  - Servers (RDP, SSH)
  - Files
  - MSSQL databases
** Access lifecycle
- Access granted upon request and approval
- Wait 2 years
- User transfers to new job, no longer needs access
- Access removed... *?*
  - Compliance and security compel this
  - Users and supervisors have *no incentive to help*
** Group lifecycle
- Group created 
  - New application, new fileshare
- Useful period
  - Poor docs, no hyperlinks
  - Purpose may shift
  - Membership rules go soggy
    - ``Please mirror [user X]'s access for [new user]''
- Sunset
  - App decommissioned, fileshare deleted
  - Companies bought, ADs mushed together
  - Group removed... *?*
* Info in hand
** Info from AD (for everybody)
- Manager
- Department
- Title
- Group memberships
- /n/-managers /(inferred)/
** Position change notifications
- Full name
- Username
- Old and new manager
- Old and new department
- Old and new job title
- Effective date
* Example organization
** Example AD users
#+BEGIN_SRC dot :file ad_users.pdf :cmdline -Kdot -Tpdf
  digraph {
  rankdir="LR"; 
  //mode="ipsep"; overlap="ipsep";
  //overlap="orthoxy";
    dse_root [label="dc=contoso,\ndc=com",shape=folder,color="blue"];
    emp_ou [label="Employees",shape=folder];

    arlinda [label="Arlinda",shape=egg];
    bob [label="Bob",shape=egg];
    chia [label="Chia",shape=egg];
    darby [label="Darby",shape=egg];
    esther [label="Esther", shape=egg];
    fabian [label="Fabian", shape=egg];

    dse_root -> emp_ou;

    emp_ou -> arlinda;
    emp_ou -> bob;
    emp_ou -> chia;
    emp_ou -> darby;
    emp_ou -> esther;
    emp_ou -> fabian;
    }
#+END_SRC

#+ATTR_LaTeX: :width 0.7\textwidth
#+RESULTS:
[[file:ad_users.pdf]]
** Example AD groups
#+BEGIN_SRC dot :file ad_groups.pdf :cmdline -Kdot -Tpdf
  digraph {
  rankdir="LR"; 
  //mode="ipsep"; overlap="ipsep";
  //overlap="orthoxy";    dse_root [label="dc=contoso,\ndc=com",shape=folder,color="blue"];
    group_ou [label="Groups", shape=folder];
    FieldFiles [label="FieldFiles", shape=invhouse];
    WeatherData [label="WeatherData", shape=invhouse];
    HandbagServer [label="HandbagServer", shape=invhouse];
    
    dse_root -> group_ou;

    group_ou -> FieldFiles;
    group_ou -> WeatherData;
    group_ou -> HandbagServer;
    }
#+END_SRC

#+RESULTS:
[[file:ad_groups.pdf]]
** Example orgchart
#+BEGIN_SRC dot :file orgchart.pdf :cmdline -Kdot -Tpdf
  digraph {
  rankdir="LR";
    arlinda [label="Arlinda\nCEO",shape=egg];
    bob [label="Bob\nVP, Handbags",shape=egg];
    chia [label="Chia\nVP, Weather",shape=egg];
    darby [label="Darby\nSenior Pocket\nDesigner",shape=egg];
    esther [label="Esther\nStrap Tech",shape=egg];
    fabian [label="Fabian\nStorm Manager", shape=egg];

    arlinda -> bob;
    arlinda -> chia;
    bob -> darby;
    bob -> esther;
    chia -> fabian;
    }
#+END_SRC

#+RESULTS:
[[file:orgchart.pdf]]
** Example position change
#+BEGIN_SRC dot :file poschange.pdf :cmdline -Kdot -Tpdf
  digraph {
  rankdir="LR";
    arlinda [label="Arlinda\nCEO",shape=egg];
    bob [label="Bob\nVP, Handbags",shape=egg];
    chia [label="Chia\nVP, Weather",shape=egg];
    darby [label="Darby\nSenior Pocket\nDesigner",shape=egg,fontcolor=gray,color=gray];
    esther [label="Esther\nStrap Tech",shape=egg];
    darby2 [label="Darby\nWeather Designer",shape=egg,fontcolor=red,color=blue];
    fabian [label="Fabian\nStorm Manager", shape=egg];

    arlinda -> bob;
    arlinda -> chia;
    bob -> esther;
    bob -> darby [color=gray];
    chia -> fabian;
    chia -> darby2;
    subgraph {
    rank=same;
    darby -> darby2 [color=blue];
    }
    }
#+END_SRC

#+RESULTS:
[[file:poschange.pdf]]
** Problem
#+LaTeX_env: small
| _User_      | _Groups_                  |
| Bob         | HandbagServer, FieldFiles |
| Darby (old) | HandbagServer, FieldFiles |
| Esther      | HandbagServer             |
| Chia        | WeatherData, FieldFiles   |
| Fabian      | WeatherData               |
| Darby (new) | *?*                       |
* Machine learning
** Machine learning
- Abandon manual rules
- "build a model ... to make predictions or decisions"
- Relevant https://xkcd.com/1838
- Deep neural networks not necessary
- Here: /supervised learning/, /classification/
** Techniques
- Logistic regression
- Decision trees
- Random forests
- *Once you know what it is, scikit-learn will do it for you*
** Workflow
- Import data in CSVs
- Improve/calculate /features/
- Fit models
- Ask models questions
- Output answers, explanations, confidences
** Features
A fixed count of mathable numbers for each observation.
*** Dummy variables
- Managers and n-managers
- Titles and departments
*** Group memberships
    :PROPERTIES:
    :BEAMER_envargs: <2->
    :END:
- How many groups exist?
- That many booleans, per user
* But...
** Did it work?
- Human-written rules abandoned; now no human-readable rules
- Cross-validation
  - Precision, recall, f1 score
- Explainability
  - Weightiest features
** Can it work better?
- Explore more models
- Change hyperparameters
- Convert information to features better
- Improve information
- Add more information
** Improve information
- Clean out groups
- Remove unused groups
- Make group meaning simpler
  - This may act against security 
** Add more information
- Which groups don't do anything anymore?
  - Fileshare groups not mentioned in any ACLs
  - Application groups for apps stood down
  - Improve group lifecycle
- What requests are made by people in certain jobs?
  - Look in ticketing system
- Are some groups requested by more senior people?
  - Obtain employee hire dates
** Ethical implications
- Concentrating data amplifies risk.
- Extrapolating amplifies bias.
- Why are things the way they are?
- What happens if we do more of the same?
** Consequences
- False positives (yes, remove user from group)
  - Hassle for users
  - Rework for security team
  - Personal experience with group
- False negatives (no, leave user in group)
  - Same as before
  - Least privilege violation
  - Legal risk
* Resources
** Sources
- scikit-learn documentation
- Wikipedia
- Youtube: StatQuest with Josh Starmer
- Hilton, Westman
- See URLs in code
** Do a thing!
- Get the code
  - https://gitlab.com/jaredjennings/learned_group_changes
  - presentation in doc/presentation/
- Contact Jared
  - jjennings@fastmail.fm
