# A map to learned\_group\_changes

## Modules

Facing the user is `__main__.py`, run with `python -m
learned_group_changes`. It creates the `ActiveDirectoryData` object
(defined in the `ad_data` module), and uses it to
`recommend_group_removals` (`recommend` module).

The code for the methods of the `ActiveDirectoryData` class, in turn,
is in the `obtain`, `calculate`, and `explain` modules. The single
`ActiveDirectoryData` instance, usually called `d`, has all the data
about `users` and `groups` in Pandas DataFrames of the same name which
are `d`'s attributes. Code to add columns to these DataFrames is in
the `user`, `group` and `user_group` modules.

## Machine-learning concepts

Broadly, `learned_group_changes` constructs `scikit-learn` models,
then asks them questions. The models find patterns in the data, then
use them to make predictive answers: sort of like linear regression,
except with yes/no values instead of continuous numbers.

To _fit_ a model, we feed it data about each of the users, and whether
each is a member of the group or not. We also _validate_ it, yielding
a _score_, to characterize its predictive power. But the data fed in
must be in the right format: a _feature vector_ which contains only
numbers. The `obtain` module, and the `user`, `group` and `user_group`
modules it calls into, make the data available in this format by
"massaging" it. `calculate` uses it to fit a model, and to score the
model. `recommend` uses the fit model (if indeed it could be fit) to
recommend groups from which to remove a moving user.

### Massaging

Most of the AD data is _categorical_, not _quantitative_. Massaging,
as we use the term here, is unlocking the predictive potential in that
categorical data.

For example, the Manager attribute has the name of a person as its
value, not a number; categories such as these are not ordered - one
manager is not more or less than another. The way to use these as an
input for classification is to create a _dummy variable_ for each
possible value: is Jared's manager Seth? (0) is Jared's manager Bob?
(0) is Jared's manager Jason? (1) and so on. These then are (hundreds
of) quantitative variables usable in model training.

Or take the Title attribute. It is certainly categorical, but some
values may not be entirely independent of others. A "Pipefitter B" is
more like a "Pipefitter A" than a "Design Engineer" is, for
example. Massaging creates numeric dummy variables that characterize a
Title; one way is to split the Title into words and make a dummy
variable for each word. Does Rudy have the word "Design" in his title?
Does Terri have the word "B" in her title? (Some of these words may be
more useful than others, and you might see how improvements could be
made.)

### Validation and fitting

[Cross-validation explained](https://scikit-learn.org/stable/modules/cross_validation.html#cross-validation).

We validate each group model. There is a tradeoff between the number
of folds used in validation, the computational difficulty of
validation, the goodness of the resulting score, and the amount of
data needed. For some groups, there are not enough members to validate
the model. In this case we assume the model is invalid.

We fit each group model. For some groups, members don't have enough in
common for the fitting process to converge. For some groups, there are
not enough members to fit the model. In these cases the model cannot
be used to predict anything.

After validation and fitting, we know for which groups we can render a
recommendation about whether a user should be a member, and an idea of
how good that recommendation might be. It's important to note that
both improvements in the massaging and improvements in the raw data
can improve the models.

### Recommendation and explanation

So then we just feed a feature vector for a moving user into each
group model and do what they say, right? Well - some of those models
were unfittable, and some of them are unfit. (See what I did there?)
And the recommendation needs to be justified from the
data. `scikit-learn` makes fitting, validation and prediction work
nearly the same for every kind of classifier it supports, but
explaining the recommendation gets a bit more into the details of the
model used.

Logistic regression finds the variables whose values contribute most
to the odds of the answer being yes or no. Each variable gets a
weight. Explaining our recommendation means finding the largest
weights, and comparing the feature vector for the moving user with
them. If a feature has a high positive weight (meaning many group
members have a 1 for this feature), and the moving user has a 1 for
this feature, and the prediction from the model is that the user
should be in the group, that feature is a big part of why. 

### Improvement

But why people are in a group may not have anything to do with their
manager or title. If all the data we give the model to go on is not
related with the right answer, the model will be inaccurate, or even
fail to converge. If a recommendation can be made, the low accuracy
score of the model means lower confidence to act automatically. To
increase confidence,

 * more data may be fed into the model (perhaps group membership will
   be related to some new type of data),
 * data may be massaged more to bring relationships to the surface
   (take n-managers as an example),
 * data may be improved (e.g. controlling the set of words used in
   titles),
 * groups may be weeded out (old groups are more likely to contain
   spurious members and more likely not to serve a purpose anymore),
 * group membership could be curated more.
 * different classifiers could be used.
 * hyperparameters could be modified.
 * validation parameters could be modified.
 
Simplicity of our groups and their membership will make their proper
function more sure and their proper use more clear, but simplicity is
not a primary goal, security and function are.

## Pandas

We use Pandas to do the data massaging. For the uninitiated, it may be
compared to Excel, in the respects that a table is a central concept,
columns may contain different kinds of data, values for a column may
be calculated from other values, and data can be filtered and sorted.

We have three big tables of data: one for user info `users`, one for
group info `groups`, and one that unifies them, `user_group`. It has
one row for each membership of a user in a group, identifying the user
and the group.

Users and groups are uniquely identified in the AD by their
distinguished names or SIDs. Both of these are too long to fit in a
column name, and in some cases they may be null. So in many places, as
a short, unique, non-nullable identifier, we use the row index of a
user or group. These, where used, are generally called `uidx` and
`gidx`. They are explicitly not valuable across runs of the script: by
our earlier analogy, they are like row numbers in Excel. Pandas
supports niftier indices, but at this writing we do not use them.

## Other conventions

In Lisp systems used early on for AI, there were functions called
_predicates_, which would take a thing and return a boolean. There was
a convention that their names would end with a _p_, and so one hacker
of the time might quip to another at midday, "Lunchp?" (to which the
answer would likely be "T"). In like manner, the boolean columns
indicating whether someone's manager is the user with (e.g.) index 42
are named with a _p_, like `uidx_mgr01_p_42`, and likewise with
n-managers (`uidx_nmgr_p_42`, is the user with index 42 anywhere above
this user in the management hierarchy?) and group memberships
(`gidx_p_1500`, is this user in the group with index 1500?).
