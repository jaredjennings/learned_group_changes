# n-managers

Active Directory stores, for each user, the identity of their
manager. When someone moves inside the organization, they will move
under a new manager. Since managers are correlated with teams, who
perform certain kinds of activities on the network and therefore need
certain access, the old and new manager are likely to be strongly
correlated to the set of group changes that should happen for a moving
employee.

But some kinds of access may not be assigned based on the smallest
organizational group of which the user is a part. Perhaps there is a
fileshare that everyone in Accounting has access to, whether they are
part of Accounts Payable, Accounts Receivable, Taxes, or what have
you. Such accesses will be correlated with being part of the
organization anywhere under a certain person. 

Maybe your manager is that person, or maybe they are your manager's
manager, or so on. Everyone above you in the organization is one of
your n-managers.

By looking for n-managers, we are implicitly expecting that everyone
has a finite number of n-managers: that the manager relation arranges
users into one or more trees having roots, which are users who have no
manager. Or, stated differently, if we call the count of users who
have a k-manager c_k_, then for some k, c_k_ is 0.


# Manager loops

But manager loops are possible. For example, if user A's manager is
user B, and user B's manager is user A, then A and B form a manager
loop. Loops may be bigger than that (e.g. A -> B -> C -> A, and so
on), and there may be multiple loops in the dataset. Trees can't have
loops. Or, stated the other way, for each loop, for all users having a
manager who's part of that loop, those users do not have a finite
number of n-managers, and so there is no k such that c_k_ is 0.


## Detecting

There may be something to do to patch up the data in a sense-making
way, but the first thing to do is to detect loops and avoid following
them forever till we run out of resources.

We would like to detect not just the first loop we find, but as many
as we can, so that they can be dealt with all at once.  So, we are not
going to quit when we find a manager loop, but we will avoid
processing anyone further who has a manager loop.

We begin by adding the 1-manager column in `add_uidx_mgr01`, and then
in `add_manager_uidx_tree` we build on the k-manager column, starting
with k=1, to come up with the k+1-manager column, until we are done.

In the code prior to this writing, "done" means that we reach a value
of k where c_k_ equals 0: there are zero users who have a
k-manager. There was one manager loop, at the very top of the
organization, and we took care of it in preprocessing. But really we
got lucky. Now there is an unknown number of loops, and they are at
different places inside the organization, and they could change
without notice.

So we have to change what "done" means. The observed behavior with the
unmodified code is that c_k_ goes down sharply, reaches 34, and stays
there indefinitely. The first attempt at a fix, then, is that we
should stop when c_k_ = c_k-1_. (This may prove to be insufficient: a
repeating but non-constant pattern could emerge in c_k_. But it hasn't
yet.) Now the problem with those 34 users is not who they are, but the
repeating set of managers being written in their k+1-manager columns.


## Remediating

First, if a user is detected to be their own k-manager, they are part
of a manager loop of size k. We set that user's k-manager to nothing
(NaN in this version of Pandas), and move on.

That only stops us iterating through manager loops for the people who
are actually part of the loop. Everyone under them will have a
k-manager the same as their i-manager, for some i less than k. We have
to check for such people, set their k-manager to nothing and move on.

With these settings for k-manager, we will stop iterating on anyone
who is part of a manager loop, or under such a person, and we'll be
able to see everybody who has problems. But this fix will result in
multiple disjoint trees of people, which will hurt the fidelity of our
model of the organization, and thus our efforts to model correlation
between group membership and position inside the organization.

With too many disjoint manager trees, groups that are common across
multiple trees won't look as common, because instead of a strong
correlation between having a single highly-ranked person as one's
n-manager and membership in the group, there will be multiple weaker
correlations between having one of a set of lower-ranked people as
one's n-manager and membership in the group.

If this happens too much, the group change suggestions could be
unacceptably degraded in quality, so we need a safety valve. The
simplest way is to write down which users have manager loop problems,
and if there are too many, quit.


## Data quality

But the problem here is having no n-managers in common, which is more
general than having manager loops; our particular fix for manager
loops is only one cause for having a disjoint organization as written
in the AD manager attributes.

So, more generally, as a measure of input data quality, we could
require (after this loop-pruning thing we're doing here) that nearly
all the users fall under one n-manager. Any user who has no manager,
and only has a few n-reports or none, should possibly be hooked up to
the organization better.

But this could be the real shape of someone's organization. Perhaps
before throwing up our hands we should better quantify how bad the
effect of this shape of data is for our efforts. And it may not be the
most important thing to fix: for example, at this writing, manual
answers, given by admins where our model lacked surety, are not folded
back into the model.
