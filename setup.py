from setuptools import setup

setup(name='learned_group_changes',
      version='0.22',
      description='Machine-learn changes to the AD group memberships of users who are transferring inside the organization.',
      author='Jared Jennings',
      author_email='jjennings@fastmail.fm',
      packages=['learned_group_changes',
                'learned_group_changes.abbreviation_lists',
                'learned_group_changes.netbsd_dict',
                'learned_group_changes.other_dictionaries',
      ],
      package_data={
          'learned_group_changes.abbreviation_lists': [
              'contoso', 'contoso_titles', 'contoso_sorta', 'contoso_typos'],
          'learned_group_changes.netbsd_dict': [
              'american', 'british', 'COPYRIGHT', 'eign',
              'propernames', 'README', 'stop', 'web2', 'web2a',
              'words', 'special/math', 'special/netbsd',
          ],
          'learned_group_changes.other_dictionaries': [
              'contoso.titles', 'contoso.words',
          ],
      },
      )
