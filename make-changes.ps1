[cmdletbinding()]

$ErrorActionPreference = "Stop"

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$env:PSModulePath = "${here};${env:PSModulePath}"
Remove-Module -ea SilentlyContinue learn-group-changes
Import-Module -Force "$here\learn-group-changes.psm1"

$FileTop = '\\contoso.com\files\Group Membership Changes'
$MergedRecommendations = "${FileTop}\group-changes.csv"

& {
    Complete-ApprovedADChanges $MergedRecommendations
    # some recommendations will likely remain that are not acted on;
    # so we leave the file
    Import-CSV $MergedRecommendations | Get-UnansweredWarnings | Write-Warning
} 3> mc-warnings.txt 4>mc-verboses.txt 6>mc-infos.txt

if((gc mc-warnings.txt | measure).count -gt 0) {
    Write-Warning "Warnings found! Emailing admins."
    gc mc-warnings.txt | Write-Warning
    Send-MailMessage -SmtpServer mail.contoso.com -To 'Admins@contoso.com' -From 'learned-group-changes@contoso.com' -Subject 'Warnings from make-changes' -Body $(gc mc-warnings.txt | Out-String)
}

if((gc mc-infos.txt | measure).count -gt 0) {
    Write-Warning "Infos found! Emailing admins."
    gc mc-warnings.txt | Write-Warning
    Send-MailMessage -SmtpServer mail.contoso.com -To 'Admins@contoso.com' -From 'learned-group-changes@contoso.com' -Subject 'Warnings from make-changes' -Body $(gc mc-warnings.txt | Out-String)
}
