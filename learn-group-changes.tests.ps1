# https://blogs.technet.microsoft.com/heyscriptingguy/2015/12/17/testing-script-modules-with-pester/

Get-Module learn-group-changes | Remove-Module -Force
Import-Module $PSScriptRoot\learn-group-changes.psm1 -Force

InModuleScope learn-group-changes {
    Function New-Recommendation($date,$sAMAccountName,$userSID,$group,$action,
                                $GroupSID,$description,$accuracy,$confirmation)
    {
        [PSCustomObject] @{
            date=$date
            sAMAccountName=$sAMAccountName
            userSID=$userSID
            group=$group
            action=$action
            GroupSID=$GroupSID
            description=$description
            accuracy=$accuracy
            confirmation=$confirmation
        }
    }


    Describe 'Export-ADGroupUserInformation' {
    }


    Describe 'Merge-CSVsInDirectory' {

        BeforeEach {
            Remove-Item -Force -ErrorAction SilentlyContinue `
              TestDrive:\it.csv
            Remove-Item -Force -Recurse -ErrorAction SilentlyContinue `
              TestDrive:\them
        }

        Context 'when the merge directory is missing' {
            It 'creates the merge directory and leaves content in the master file' {
                [PSCustomObject] @{ one=1; two='two'; three=3 } | export-csv it.csv
                Test-Path TestDrive:\them | Should -Be $False
                Merge-CSVsInDirectory `
                  TestDrive:\them `
                  TestDrive:\it.csv
                Test-Path TestDrive:\them | Should -Be $True
                @(import-csv it.csv).count | Should -Be 1
            }
            It 'creates a missing master file' {
                Test-Path -PathType Leaf TestDrive:\it.csv | Should -Be $False
                Merge-CSVsInDirectory `
                  TestDrive:\them `
                  TestDrive:\it.csv
                Test-Path -PathType Leaf TestDrive:\it.csv |
                  Should -Be $True
                import-csv TestDrive:\it.csv | Should -BeNullOrEmpty
            }
        }

        Context 'when the merge directory exists' {
            BeforeEach {
                mkdir TestDrive:\them
            }
            It 'creates a missing master file' {
                Test-Path -PathType Container TestDrive:\them |
                  Should -Be $True
                Test-Path -PathType Leaf TestDrive:\it.csv |
                  Should -Be $False
                Merge-CSVsInDirectory `
                  TestDrive:\them `
                  TestDrive:\it.csv
                Test-Path -PathType Container TestDrive:\them |
                  Should -Be $True
                Test-Path -PathType Leaf TestDrive:\it.csv |
                  Should -Be $True
                import-csv TestDrive:\it.csv | Should -BeNullOrEmpty
            }
            It 'merges zero files properly' {
                [PSCustomObject] @{ one=1; two='two'; three=3 } |
                  export-csv TestDrive:\it.csv                
                Merge-CSVsInDirectory `
                  TestDrive:\them `
                  TestDrive:\it.csv
                # we should not have lost this row, nor gained any
                @(import-csv TestDrive:\it.csv).count | Should -Be 1
            }
            Context 'when the merge directory has a file' {
                BeforeEach {
                    [PSCustomObject] @{ one=8; two='bnw'; three=41} |
                      export-csv TestDrive:\them\1.csv
                }
                It 'merges files into an empty master file' {
                    Merge-CSVsInDirectory `
                      TestDrive:\them `
                      TestDrive:\it.csv
                    $after_merge = @(Import-CSV TestDrive:\it.csv)
                    $after_merge.count | Should -Be 1
                    $columns = $after_merge | get-member | ? { $_.membertype -eq 'NoteProperty' }
                    $columns.count | Should -Be 3
                }
                It 'adds rows to the master file' {
                    [PSCustomObject] @{ one=1; two='two'; three=3 } |
                      export-csv TestDrive:\it.csv
                    @(import-csv it.csv).count | Should -Be 1
                    Merge-CSVsInDirectory `
                      TestDrive:\them `
                      TestDrive:\it.csv
                    @(import-csv TestDrive:\it.csv).count | Should -Be 2
                }
            }

            Context 'when the merge directory has a file with different columns' {
                BeforeEach {
                    [PSCustomObject] @{ zart=8; two='bnw'; three=41} |
                      export-csv TestDrive:\them\different.csv
                }
                It 'adds rows to the master file' {
                    [PSCustomObject] @{ one=1; two='two'; three=3 } |
                      export-csv TestDrive:\it.csv
                    @(import-csv it.csv).count | Should -Be 1
                    Merge-CSVsInDirectory `
                      TestDrive:\them `
                      TestDrive:\it.csv
                    @(import-csv TestDrive:\it.csv).count | Should -Be 2
                }
                It 'does not add columns to the master file' {
                    [PSCustomObject] @{ one=1; two='two'; three=3 } |
                      export-csv TestDrive:\it.csv
                    @(import-csv it.csv).count | Should -Be 1
                    Merge-CSVsInDirectory `
                      TestDrive:\them `
                      TestDrive:\it.csv
                    $after_merge = @(Import-CSV TestDrive:\it.csv)
                    $after_merge.count | Should -Be 2
                    $columns = $after_merge | get-member | ? { $_.membertype -eq 'NoteProperty' }
                    $columns.count | Should -Be 3
                }
            }

            Context 'when the merge directory has several files in it' {
                BeforeEach {
                    @(
                        [PSCustomObject] @{ one=1; two='two'; three=3 },
                        [PSCustomObject] @{ one=6; two='qwo'; three=8 },
                        [PSCustomObject] @{ one=11;two='wty'; three=40}) |
                          Export-CSV TestDrive:\it.csv
                    @(
                        [PSCustomObject] @{ one=8; two='bnw'; three=41},
                        [PSCustomObject] @{ one=9; two='dsh'; three=42},
                        [PSCustomObject] @{ one=12;two='qof'; three=43}) |
                          Export-CSV TestDrive:\them\more-rows-same-cols.csv
                    @(
                        [PSCustomObject] @{ one=14;two='ywf'; three=45},
                        [PSCustomObject] @{ one=16;two='qlf'; three=45},
                        [PSCustomObject] @{ one=18;two='fwy'; three=45}) |
                          Export-CSV TestDrive:\them\more2-rows-same-cols.csv
                    @(
                        [PSCustomObject] @{ zart=8; two='bnw'; three=41},
                        [PSCustomObject] @{ zart=9; two='dsh'; three=42},
                        [PSCustomObject] @{ zart=12;two='qof'; three=43}) |
                          Export-CSV TestDrive:\them\different-cols.csv
                    }
                It 'merges all the files into the master file' {
                    @(import-csv TestDrive:\it.csv).count | Should -Be 3
                    Merge-CSVsInDirectory `
                      TestDrive:\them `
                      TestDrive:\it.csv
                    $after_merge = @(Import-CSV TestDrive:\it.csv)
                    $after_merge.count | Should -Be 12
                    $columns = $after_merge | get-member | ? { $_.membertype -eq 'NoteProperty' }
                    $columns.count | Should -Be 3
                }
                It 'does not write type information' {
                    @(import-csv TestDrive:\it.csv).count | Should -Be 3
                    Merge-CSVsInDirectory `
                      TestDrive:\them `
                      TestDrive:\it.csv
                    gc TestDrive:\it.csv -First 1 | Should -not -BeLike '#TYPE *'
                }
                It 'deletes the merged files' {
                    @(Get-ChildItem TestDrive:\them).count | Should -Be 3
                    Merge-CSVsInDirectory `
                      TestDrive:\them `
                      TestDrive:\it.csv
                    @(Get-ChildItem TestDrive:\them).count | Should -Be 0
                }
            }
        }
    }
    

    Describe 'Complete-ApprovedADChanges' {
        Mock Export-GroupMembership {}
        Mock Remove-ADGroupMember {}
        Mock Add-ADGroupMember {}
        Mock Add-Content {}
        Mock Get-Date { [DateTime] '2019-09-06 00:04:02' }
        It 'does not do anything with changes whose time has not yet come' {
            @(
                New-Recommendation '2019-09-10 00:00:00' 'e_g_user1' 'user-sid' `
                  'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                  0.1 'y'
            ) | export-csv TestDrive:\recommendations.csv
            Complete-ApprovedADChanges TestDrive:\recommendations.csv
            Assert-MockCalled Remove-ADGroupMember -Scope It -Exactly 0
        }
        It 'forgets rows with disapproved changes' {
            @(
                New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                  'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                  0.1 'n'
            ) | export-csv TestDrive:\recommendations.csv
            Complete-ApprovedADChanges TestDrive:\recommendations.csv
            (Import-CSV TestDrive:\recommendations.csv) | Should -BeNullOrEmpty
        }
        It 'keeps rows which have not been approved or disapproved' {
            @(
                New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                  'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                  0.1 '?'
            ) | export-csv TestDrive:\recommendations.csv
            Complete-ApprovedADChanges TestDrive:\recommendations.csv
            (Import-CSV TestDrive:\recommendations.csv) | Should -Not -BeNullOrEmpty
        }
        It 'forgets rows with approved and accomplished changes' {
            @(
                New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                  'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                  0.1 'y'
            ) | export-csv TestDrive:\recommendations.csv
            Complete-ApprovedADChanges TestDrive:\recommendations.csv
            (Import-CSV TestDrive:\recommendations.csv) | Should -BeNullOrEmpty
        }
        It 'keeps rows with garbled confirmation' {
            @(
                New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                  'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                  0.1 'whaaat is this confirmation value'
            ) | export-csv TestDrive:\recommendations.csv
            Complete-ApprovedADChanges `
              TestDrive:\recommendations.csv `
              -warningvariable warn -warningaction silentlycontinue
            (Import-CSV TestDrive:\recommendations.csv) | Should -Not -BeNullOrEmpty
            # "NOTE: The WarningVariable parameter does not capture warnings from
            # nested calls in functions or scripts." -_-
        }
        Context 'when changes fail' {
            Mock Remove-ADGroupMember {
                Throw "i aim to misbehave while removing"
            }
            Mock Add-ADGroupMember {
                Throw "i aim to misbehave while adding"
            }
            It 'keeps rows with approved changes' {
                # this one does not use the exported function. but it
                # is so nice and short isn't it
                $before = @(
                    New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                      'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                      0.1 'y'
                )
                $w = @()
                $after = $before |
                  Complete-ApprovedADChange -WarningVariable w 3>$null
                $after | should -be $before
                $w.count | Should -BeGreaterThan 0
                # message should contain...
                # what we were trying to do
                $w[0] | should -BeLike "* remove*"
                # for whom
                $w[0] | should -BeLike "* e_g_user1*"
                # to what group
                $w[0] | should -BeLike "* Gawpers*"
                # what the error was
                $w[0] | should -BeLike "* misbehave*"
            }
            It 'keeps going' {
                @(
                    New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                      'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                      0.1 'y'
                    New-Recommendation '2019-09-06 00:00:00' 'goo_user2' 'goo-sid' `
                      'Gawpers' 'add' 'some-sid' 'position change blibli' `
                      0.1 'y'
                ) | export-csv TestDrive:\recommendations.csv
                $warn = @()
                Complete-ApprovedADChanges `
                  TestDrive:\recommendations.csv `
                  -warningvariable warn 3>$null
                (Import-CSV TestDrive:\recommendations.csv).count | Should -Be 2
            }
        }
        It 'removes a user from a group when suggested and approved' {
            @(
                New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                  'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                  0.1 'y'
            ) | export-csv TestDrive:\recommendations.csv
            Complete-ApprovedADChanges TestDrive:\recommendations.csv
            Assert-MockCalled Remove-ADGroupMember -Scope it -Exactly 1 `
              -parameterfilter { ($identity -like 'some-sid') -and `
              ($members -like 'user-sid') }
        }
        # should this happen?
        It 'exports group memberships of a user once before changing them' {
            @(
                New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                  'Gawpers' 'remove' 'some-sid' 'position change blabla' `
                  0.1 'y'
                New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                  'Yawpers' 'remove' 'some-sid' 'position change blabla' `
                  0.1 'y'
            ) | export-csv TestDrive:\recommendations.csv
            Complete-ApprovedADChanges TestDrive:\recommendations.csv
            Assert-MockCalled Export-GroupMembership -Scope it -Exactly 1
        }
        It 'adds a user to a group when suggested and approved' {
            @(
                New-Recommendation '2019-09-06 00:00:00' 'e_g_user1' 'user-sid' `
                  'Gawpers' 'add' 'some-sid' 'position change blabla' `
                  0.1 'y'
            ) | export-csv TestDrive:\recommendations.csv
            Complete-ApprovedADChanges TestDrive:\recommendations.csv
            Assert-MockCalled Add-ADGroupMember -Scope it -Exactly 1 `
              -parameterfilter { ($identity -like 'some-sid') -and `
              ($members -like 'user-sid') }
        }
    }
    Describe 'Get-UnansweredWarnings' {
        Context 'when it is Sept 6, 2019' {
            Mock Get-Date { [DateTime] '2019-09-06 00:04:02' }
            It 'says nothing when there are no questions the user needs to answer' {
                @(
                    [PSCustomObject] @{
                        group='a'
                        action='remove'
                        sAMAccountName='b'
                        confirmation='y'
                        description='bla'
                        date='2019-09-16 00:00'
                        accuracy='0.8'
                        UserSID='usid'
                        GroupSID='gsid'
                    }) |
                      Export-CSV TestDrive:\no-questions.csv
                $out = Import-CSV TestDrive:\no-questions.csv |
                  Get-UnansweredWarnings
                $out | Should -BeNullOrEmpty
            }
            It 'returns some text when there are questions the user needs to answer' {
                @(
                    [PSCustomObject] @{
                        group='a'
                        action='remove'
                        sAMAccountName='b'
                        confirmation='y?'
                        description='bla'
                        date='2019-09-16 00:00'
                        accuracy='0.8'
                        UserSID='usid'
                        GroupSID='gsid'
                    }) |
                      Export-CSV TestDrive:\no-questions.csv
                $out = Import-CSV TestDrive:\no-questions.csv |
                  Get-UnansweredWarnings
                $out.count | Should Be 1
                ($out | out-string) | Should -BeLike '*uncertain group changes*'
            }
            It 'chides users when they are overdue in answering questions' {
                @(
                    [PSCustomObject] @{
                        group='a'
                        action='remove'
                        sAMAccountName='b'
                        confirmation='y?'
                        description='bla'
                        date='2019-09-01 00:00' <# #>
                        accuracy='0.8'
                        UserSID='usid'
                        GroupSID='gsid'
                    }) |
                      Export-CSV TestDrive:\no-questions.csv
                $out = Import-CSV TestDrive:\no-questions.csv |
                  Get-UnansweredWarnings
                $out.count | Should Be 2
                ($out | out-string) | Should -BeLike '*would immediately*'
            }
        }
    }

    Describe 'Get-ADLikelyPeople' {
        Mock Get-ADUser {}

        It 'fetches the mail and lastLogonDate' {
            Get-ADLikelyPeople -verbose
            Assert-MockCalled Get-ADUser -exactly 1 -scope it `
              -parameterfilter {
                  'mail' -in $Properties -and `
                    'lastLogonDate' -in $Properties }
        }
        It 'fetches other properties in addition when asked' {
            Get-ADLikelyPeople -verbose -properties office,title,manager
            Assert-MockCalled Get-ADUser -exactly 1 -scope it `
              -parameterfilter {
                  'mail' -in $Properties -and `
                    'lastLogonDate' -in $Properties -and `
                    'office' -in $Properties }
        }
        It 'normally does not include idle users' {}
        It 'includes idle users when -IncludeIdle is given' {}
        It 'forwards switches to Get-ADUser' {
            Get-ADLikelyPeople -SearchBase foo
            Assert-MockCalled Get-ADUser -exactly 1 -scope it `
              -parameterfilter { $SearchBase -eq 'foo' }
        }
    }
}
