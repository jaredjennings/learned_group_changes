function Export-GroupMemberships {
    <#
.SYNOPSIS

Save the set of groups someone was a member of at a certain time,
usually before doing something drastic to that set.

#>
    param(
        [Parameter(mandatory=$true)]
        $user_identifier,
        $BygoneGroupMembershipsCSV)
    $now = get-date -format s   # ISO-8601
    $u = get-aduser $user_identifier -property DistinguishedName,MemberOf
    $u.MemberOf | % {
        [PSCustomObject] @{
            When=$now
            UserDN=$u.DistinguishedName
            GroupDN=$_ }
    } | export-csv `
      -Append -Path $BygoneGroupMembershipsCSV `
      -NoTypeInformation
}


function Get-IgnoredADGroupDNs {
    <#
.SYNOPSIS
Get the list of distinguished names of Active Directory groups that we
never want to bother with for position change purposes.

.DESCRIPTION
Criteria for being listed here:

* Super-secret groups, about which info cannot be gotten
* Groups associated closely with particular people, not merely the jobs they do
* Groups most everyone goes in

#>
    $ignore_group_dns = @(
        'CN=HR,OU=Groups,DC=contoso,DC=com'
        'CN=VPN Access,OU=Groups,DC=contoso,DC=com'
        'CN=Domain Admins,CN=Builtin,DC=contoso,DC=com'

    ) + (get-adgroup -searchbase 'CN=special,DC=contoso,DC=com' `
      -filter * -property canonicalname |
      sort canonicalname |
      % { $_.distinguishedname })

    $ignore_group_dns
}


function Get-ADLikelyPeople {
    <#

.SYNOPSIS

Get AD users that are probably people.

.DESCRIPTION

This is where we filter out meeting rooms, service accounts, and
other things that are users but not people.

.PARAMETER IncludeIdle

If you give this switch, users whose LastLogonDate is more than 30
days ago will be included in the output. Otherwise they will not be.

.EXAMPLE

Get-ADLikelyPeople -SearchBase 'ou=Users,dc=contoso,dc=com' `
  -IncludeIdle -Properties title,office,manager,officephone

.EXAMPLE

Get-ADLikelyPeople | Measure-Object

#>
    [CmdletBinding()]
    param(
        [Switch] $IncludeIdle,
        [Parameter(ValueFromRemainingArguments=$true)]
        $rest=@())
    
    Begin {
        $now = Get-Date
        if($IncludeIdle) {
            function Filter-ByIdleness {
                [CmdletBinding()]
                param([Parameter(ValueFromPipeline)] $u)
                process {
                    $u
                }
            }
        } else {
            function Filter-ByIdleness {
                [CmdletBinding()]
                param([Parameter(ValueFromPipeline)] $u)
                process {
                    $u | where-object { $_.lastLogonDate -gt $now.AddDays(-30) }
                }
            }
        }
        # Convert vars to hashtable. https://stackoverflow.com/a/27765140
        # Treat '-property' and '-properties' as one, and merge values
        # given. This is so you can pass to this cmdlet any parameter
        # that you would pass to Get-ADUser.
        $htvars = @{
            Properties = @('mail','lastLogonDate')
        }
        @($rest) | % {
            if($_ -match '^-') {
                # new parameter
                $lastvar = $_ -replace '^-',''
                if($lastvar -eq 'property') {
                    $lastvar = 'Properties'
                }
                if(!$htvars.containskey($lastvar)) {
                    $htvars[$lastvar] = $null
                }
            } else {
                if($lastvar) {
                    # value
                    if($lastvar -eq 'Properties') {
                        $htvars[$lastvar] += $_
                    } else {
                        $htvars[$lastvar] = $_
                    }
                } else {
                    # value without a -name first
                    $posvars += $_
                }
            }
        }
        if($posvars.count -gt 0) {
            throw "$($posvars.count) unrecognized positional parameters ($($posvars[0])...)"
        }
    }

    Process {
        get-aduser -filter {(enabled -eq $True) -and (givenname -like "*") -and (surname -like "*")} @htvars | Filter-ByIdleness
    }
}


function Export-ADGroupUserInformation {
    <#
.SYNOPSIS
Get information about all users and groups in AD, and export it to CSV files.

.DESCRIPTION

This gets all the users, all the groups, and all the memberships of
users in groups and writes each to a CSV file. It also does a little
cleanup: it doesn't emit any groups with no users in them, because
it's impossible to machine-learn anything about those at this time;
and it clears out the CEO's manager attribute on the way through, so
that the manager relation between users will not have any (known)
cycles.
#>
    param(
        [Parameter(Mandatory=$true)]
        $UserCSV,
        [Parameter(Mandatory=$true)]
        $GroupCSV,
        [Parameter(Mandatory=$true)]
        $UserGroupCSV,
        $ceo_samaccountname='morgan7',
        $ignore_group_dns=$null
    )

    if($ignore_group_dns -eq $null) {
        $ignore_group_dns = Get-IgnoredADGroupDNs
    }

    get-adlikelypeople -property manager,title,department,givenname,surname |
      % { if($_.samaccountname -eq $ceo_samaccountname) { $_.manager = $null }; $_ } |
      select SID,DistinguishedName,sAMAccountName,Manager,Title,Department,GivenName,Surname |
      export-csv -notypeinformation -encoding utf8 $UserCSV

    # keep track of groups that people are in: this forms a subset of all
    # groups; groups not in the subset are uninteresting for this purpose,
    # so filtering them out is desirable.
    $groups=@{}

    get-adlikelypeople -property memberof | % {
        $u=$_; $u.memberof | % {
            $gdn=$_
            if($gdn -notin $ignore_group_dns) {
                $groups[$gdn]=1
                [PSCustomObject] @{
                    DistinguishedName=$u.distinguishedname
                    sAMAccountName=$u.sAMAccountName
                    SID=$u.SID
                    MemberOfGroupWithDN=$gdn
                }
            } else {
                # write-warning "ignoring group membership $gdn for $($u.samaccountname)"
            }
        }
    } | export-csv -notypeinformation -encoding utf8 $UserGroupCSV

    $groups.GetEnumerator() |
      % { get-adgroup $_.key } |
      select Name,DistinguishedName,SID |
      export-csv -notypeinformation -encoding utf8 $GroupCSV
}


function Merge-CSVsInDirectory {
    <#
.SYNOPSIS
Merge all the CSV files in MergeDirectory into MasterFile, and remove them.

.DESCRIPTION

When you have a script that puts out CSV values that should be
appended to some existing values, there are many corner cases to deal
with. There are already two such scripts, with more likely to be
made. This command factors the complexity involved in dealing with
those cases out of those scripts.

Each script writing CSV data to be added should make a new CSV file
each time it has some data, with a unique filename, and write it into
a merge directory. This command is used to merge all the rows from
such files into a master file. If the master file contains data, the
new data is appended; if it is empty or missing, it is created as
necessary. This command also creates the given merge directory if it
does not exist. In such a case, of course, no data will be added to
the master file.

If the master file exists and has columns, the set of columns in it
will not change. If not, the set of columns from the first file in the
merge directory will be used.

#>
    [CmdletBinding()]
    param(
        $MergeDirectory,
        $MasterFile
    )
    if(!(Test-Path -PathType Leaf $MasterFile)) {
        Write-Verbose "MasterFile $MasterFile does not exist; creating"
        New-Item $MasterFile
    }
    if(!(Test-Path -PathType Container $MergeDirectory)) {
        Write-Verbose "MergeDirectory $MergeDirectory does not exist; creating"
        mkdir $MergeDirectory
    }
    $CSVs = @(Get-ChildItem -Recurse $MergeDirectory -Include '*.csv')
    if($CSVs.Count -gt 0) {
        $out = New-TemporaryFile
        # an experiment has confirmed that the set of columns from the
        # first file is forced upon the rest of the imports.
        (@($MasterFile) + $CSVs) |
          % { Import-CSV -ErrorAction Stop $_ } |
          Export-CSV -ErrorAction Stop -NoTypeInformation $out
        Move-Item -Force -ErrorAction Stop $out $MasterFile
        Remove-Item $CSVs
        $AllCSVNames = ($CSVs | % { $_.name }) -join ', '
        Write-Verbose "Merged into $MasterFile, and removed, the following CSVs: $AllCSVNames"
    } else {
        Write-Verbose "No CSVs to merge in; $MasterFile unaffected"
    }
}


function Complete-ApprovedADChange {
    <#
.SYNOPSIS
Do a change to AD group memberships indicated by an object, and maybe
re-emit the object.

.DESCRIPTION

The $Change parameter (q.v.) indicates a change to make in AD, either
to remove a user from a group or to add the user. It also indicates a
date on or after which the change should take place, and whether the
change is approved. This command does the Right Thing with a single
change object.

This command also filters change objects. If a change is approved and
accomplishing the change works properly, the change is not emitted. If
a change is disapproved, the change is not emitted. Otherwise the
change is emitted. The idea is you can pipe a stream of CSV row
objects into this command, and it will emit back only rows that need
to be kept in the file because they are not fully dealt with; then you
can save those and overwrite the file with them when you're done.

.PARAMETER IgnoreAfter

Ignore changes with a date after the value of this parameter. Defaults
to now.

.PARAMETER Change

An object with the following attributes:

* Confirmation. If this is neither true nor 'y', the change is ignored.
  If it is 'n' or false, the change is ignored and not emitted.
* Date. If this is in the future relative to the IgnoreAfter time, the
  change is ignored.
* Action. Either 'add' or 'remove'.
* UserSID. The SID of a user.
* GroupSID. The SID of a group.
* sAMAccountName. Used in log messages as a username.
* Group. Used in log messages as a group name.

#>
    [CmdletBinding()]
    param(
        [DateTime]
        $IgnoreAfter=(Get-Date),
        [Parameter(ValueFromPipeline=$true)]
        $Change
    )
    begin {
        $Total = 0
        # https://ss64.com/ps/syntax-arrays.html
        $exported_users = [System.Collections.ArrayList] @()
    }
    process {
        $keep = $false
        # a true value in the remove column means remove user from group.
        if($Change.confirmation -eq 'y' -or $Change.confirmation -eq $true) {
            If ([datetime]$Change.date -le $IgnoreAfter) {
                if(!($Change.UserSID -in $exported_users)) {
                    Export-GroupMembership $Change.UserSID
                    # son of a !&@#$*& this cost me THREE HOURS vvvvvv
                    $exported_users.add($Change.UserSID) >$null
                }
                # this is needed because Powershell doesn't have try/catch/else
                $action_success = $false
                try {
                    if($Change.action -eq 'add') {
                        Add-ADGroupMember $Change.GroupSID `
                          -members $Change.UserSID `
                          -erroraction Stop
                    } elseif($Change.action -eq 'remove') {
                        Remove-ADGroupMember $Change.GroupSID `
                          -members $Change.UserSID `
                          -Confirm:$False `
                          -erroraction Stop
                    } else {
                        throw "Unclear action $($Change.action) for user $($Change.sAMAccountName) ($($Change.UserSID)). Expected 'add' or 'remove'. Please fix."
                    }
                    $action_success = $true
                } catch {
                    Write-Warning "Failed to $($Change.action) user $($Change.sAMAccountname) ($($Change.UserSID)) to/from group $($Change.group) ($($Change.GroupSID)): $_. Will try again next run."
                }
                if($action_success) {
                    add-content ./Logs/Log.txt "$(get-date) - $($Change.action) $($Change.sAMAccountName) to/from $($Change.group): $($Change.description). User SID: $($Change.UserSID). Group SID: $($Change.GroupSID)."
                    $Total = $Total + 1
                } else {
                    # try again next time
                    $keep = $true
                }
            } Else {
                # future action
                $keep = $true
            }
        } elseif($Change.confirmation -eq 'n' -or $Change.confirmation -eq $false) {
            # a false confirmation value means definitively do not do
            # the thing this row says to. but that means the row
            # should not be in this file. even if it is for the future.
            $keep = $false
        } elseif($Change.confirmation -like '*`?') {
            # recommendation/question, not decided by a person yet
            $keep = $true
        } else {
            # not sure what
            Write-Warning "Could not understand confirmation value >>> $($Change.confirmation) <<<; expected y, n, true, false, or something ending with a question mark. Keeping row but not acting on it."
            $keep = $true
        }
        if($keep) {
            $Change
        }
    }
    end {
        $users_string = $exported_users -join ", "
        Write-Information "There were $Total group membership changes. Users affected by changes: $users_string."
    }
}


function Complete-ApprovedADChanges {
    <#

.SYNOPSIS

Do all the AD changes indicated in MasterFile, ignoring those whose
dates are after IgnoreAfter.


.DESCRIPTION

Each row in the MasterFile, a CSV file, indicates a change to make in
AD: either to remove a user from a group or to add a user to a
group. It also indicates a date on or after which the change should
take place, and whether the change is approved. 

This command accomplishes changes which are approved and whose date
has passed, and if that works, removes them from the MasterFile. It
removes changes which are disapproved. It keeps changes which have not
yet been approved nor disapproved, and changes which are yet to be
made in the future.


.PARAMETER MasterFile

A CSV file with at least the following columns:

* Confirmation. If this is neither true nor 'y', the change is ignored.
  If it is 'n' or false, the change is ignored and removed. The condition
  of being neither approved nor disapproved is indicated with a string
  ending in '?'; optionally it may contain a hint, e.g. 'y?' or 'oh, eh?'
* Date. If this is in the future relative to the IgnoreAfter time, the
  change is ignored.
* Action. Either 'add' or 'remove'.
* UserSID. The SID of a user.
* GroupSID. The SID of a group.
* sAMAccountName. Used in log messages as a username.
* Group. Used in log messages as a group name.


.PARAMETER IgnoreAfter

Ignore (and keep) changes with a date after the value of this
parameter. Defaults to now.

#>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        $MasterFile,
        $IgnoreAfter=(Get-Date))
    begin {
        $TempFile = New-TemporaryFile
    }
    process {
        Import-CSV $MasterFile |
          Complete-ApprovedADChange -IgnoreAfter $IgnoreAfter |
          Export-CSV $TempFile -NoTypeInformation
        Move-Item -Force $TempFile $MasterFile
    }
}

function Get-UnansweredWarnings {
    <#
.SYNOPSIS

Find warnings we should issue to Cyber users about questionable group
changes they need to resolve.

.DESCRIPTION

Returns zero or more strings, suitable for emailing to humans, which
warn about unresolved uncertain group changes in $MasterFile.

.EXAMPLE

Import-CSV group-changes.csv | Get-UnansweredWarnings | Out-Warning

#>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        $MasterFileCSVData)
    begin {
        $now = Get-Date
        $unanswered = 0
        $unanswered_past = 0
    }
    process {
        foreach($row in $MasterFileCSVData) {
            if($row.action -eq 'remove' -and $row.confirmation -match '\?$') {
                $unanswered += 1
                if([DateTime] $row.date -lt $now) {
                    $unanswered_past += 1
                }
            }
        }
    }
    end {
        if($unanswered -gt 0) {
            "There are ${unanswered} uncertain group changes."
            if($unanswered_past -gt 0) {
                "${unanswered_past} would immediately happen if answered."
            }
        }
    }
}

Export-ModuleMember @(
    'Export-ADGroupUserInformation'
    'Merge-CSVsInDirectory'
    'Complete-ApprovedADChanges'
    'Get-UnansweredWarnings'
)
