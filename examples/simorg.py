import random
from random import (
    choice as rch, choices as rchs,
    sample as rs, randrange as rr)

baseDN = 'dc=contoso,dc=com'

OUs = [
    'ou=Employees,dc=contoso,dc=com',
    'ou=Contractors,dc=contoso,dc=com',
]

department_words = '''reliability fungibility effervescence oneitude 
colditude safety cool books handbags chips stickers evangelism parking
fashion construction trust function permits water closure graffiti
lighting prevarication commitment patterns items punctuality
gadgetry redundancy community customer'''.split()

title_adjectives = '''new small legacy red cloud client'''.split()
title_nouns = '''stoker painter loitering driver designer engineer defenestrator
listener grottoman checker terminator guard mason '''.split()

depth_adj = [
    '''senior chief executive grand'''.split(),
    '''managing associate vice'''.split(),
    '''directing division deputy'''.split(),
    '''group senior scheming line'''.split(),
    '''technical tech team assistant'''.split(),
    '''probationary starter interning'''.split()]

depth_noun = [
    '''president officer pooh-bah'''.split(),
    '''director vizier'''.split(),
    '''manager supervisor'''.split(),
    '''lead henchperson'''.split(),
    '''minion underling'''.split(),
    '''marketroid technician intern'''.split()]
    

thing_words = '''acme star dark bus track joy trim time book box'''.split()

def make_username(first, last):
    return last[:3] + first[:2] + '{:03d}'.format(random.randint(0,999))

def make_title(*dept):
    return ' '.join([rch(worky_words),
                     rch([dept[0]]*3 + title_adjectives),
                     rch(title_nouns)])

def make_dept():
    return [rch(department_words) for x in range(2)]

class User:
    first_names = '''Deshawn Kailee Cecelia Marely Bria Isabel Makhi
    Karly Erika Tamia Rhianna Ahmed Trystan Abby Litzy Catherine
    Triston Emilia Hassan Aaron Liam Rowan Landon Alani
    Jared Alberto Moises Ismael Gloria Ildefonso Ilio Leonardo
    Fabio Luciano Ron Carmen Alejandra Ruben Sleve Onson Darryl
    Anatoli Rey Glenallen Mario Raul Kevin Tony Bobson Willie
    Jeromy Scott Shown Dean Mike Tim Karl Todd'''.split()

    last_names = '''Parsons Garrett Rojas Keith Blackwell Stanton
    Maynard Boyer Shea Leon Gross Barr Gentry Jacobson Christensen
    Acevedo Silva Johnston Donovan Valdez Lopez Hodges Bright Stewart
    Banks George Swanson Grimes Splockings McDichael Sweemey Archideld
    Smorin McSriff Mixon Chamgerlain Nogilny Smehrik Dugnutt Dustice
    Dourque Furcotte Wesrey Truk Rortugal Sandaele Dandleton Bonzalez
    Vandastrophe'''.split()
    
    def __init__(self, manager, department, title):
        self.manager = manager
        self.depth = 0
        m = manager
        while m is not None:
            self.depth += 1
            m = m.manager
        name = (rch(self.first_names), rch(self.last_names))
        self.fullName = ' '.join(name)
        self.givenName = name[0]
        self.sn = name[1]
        ou = rch(OUs)
        self.distinguishedName = f"cn={self.fullName},{ou}"
        self.manager = manager
        self.userName = make_username(*name)
        self.department = department
        self.title = title

    def __repr__(self):
        return '<User ' + repr(self.__dict__) + '>'

    def orgchart(self):
        indent = '  ' * self.depth
        yield f'{indent}{self.fullName}, {self.title}, {self.department}'

class Organization:
    def __init__(self):
        self.users = []
        self.ceo = User(None, 'CEO', 'CEO')
        self.teams = {}
        self.users.append(self.ceo)

    def hire_team(self, manager, team_size):
        depth = manager.depth
        if depth < 1:
            manager_dept_words = ()
        else:
            manager_dept_words = tuple(manager.department.split(' '))
        if depth < 2:
            used_dept_words = set()
            for u in self.users:
                for word in u.department.split(' '):
                    used_dept_words.add(word)
            underling_dept_wordss = []
            for i in range(team_size):
                udw = rs([x for x in department_words
                          if x not in used_dept_words], k=1)[0]
                underling_dept_wordss.append(
                    manager_dept_words + (udw,))
                used_dept_words.add(udw)
        else:
            # don't let departments specialize too much
            underling_dept_wordss = (manager_dept_words,) * team_size
        if depth < 3:
            # higher titles in the orgchart seem to come before what
            # they are about, like "vice president of reliability"
            titles = [' '.join(
                rs(depth_adj[depth], k=rr(0,2)) +
                rs(depth_noun[depth], k=1) +
                ([rch(title_adjectives)]
                 if (depth > 2) and (rr(0,10) > 3)
                 else []) +
                list(udws[:rchs(range(len(udws)+1),
                                weights=[1,2,2,1,1,1,1][:len(udws)+1],
                                k=1)[0]]) +
                []) for udws in underling_dept_wordss]
        else:
            # while lower titles come after what they are about, like
            # "handbag guard"
            titles = [' '.join(
                rs(depth_adj[depth], k=rr(0,2)) +
                ([rch(title_adjectives)] if rr(0,10) > 5 else []) +
                list(udws[:rchs(range(len(udws)+1),
                                weights=[0,5,2,1,1,1,1][:len(udws)+1],
                                k=1)[0]]) +
                rs(title_nouns, k=1) +
                []) for udws in underling_dept_wordss]
        users = [User(manager, ' '.join(udws), t)
                 for udws, t in zip(underling_dept_wordss, titles)]
        self.users.extend(users)
        self.teams.setdefault(manager.userName, [])
        self.teams[manager.userName].extend(users)

    def orgchart(self, from_user=None):
        if from_user is None:
            yield from self.orgchart(self.ceo)
        else:
            yield from from_user.orgchart()
            u = from_user.userName
            if u in self.teams:
                for u in self.teams[u]:
                    yield from self.orgchart(u)


if __name__ == '__main__':
    org = Organization()
    org.hire_team(org.ceo, 5)
    for x in range(15):
        org.hire_team(rch(org.users), 3)
    for l in org.orgchart():
        print(l)
