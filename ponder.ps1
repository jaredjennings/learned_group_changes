[cmdletbinding()]
param()

# https://github.com/PowerShell/PowerShell/issues/4568
$ErrorActionPreference = "Stop"

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$env:PSModulePath = "${here};${env:PSModulePath}"
Remove-Module -Verbose:$False -ea SilentlyContinue learn-group-changes
Import-Module -Verbose:$False -Force "$here\learn-group-changes.psm1"

$FileTop = '\\files.contoso.com\learned_group_changes\top'
$PosChangesToMerge = "${FileTop}\position-changes.d"
$MergedChanges = "${FileTop}\position-changes.csv"
$OldMergedChanges = "${FileTop}\position-changes-old.csv"
$RecommendationsToMerge = "${FileTop}\recommendations.d"
$ThisRecommendationFilename = "lgc-rec-$(get-date -UFormat '%Y%m%d-%H-%M-%S-')-$(Get-Random).csv"
$ThisRecommendation = "${RecommendationsToMerge}\${ThisRecommendationFilename}"
$MergedRecommendations = "${FileTop}\group-changes.csv"

& {
    Export-ADGroupUserInformation `
      -UserCSV ad-user.csv `
      -GroupCSV ad-group.csv `
      -UserGroupCSV ad-user-group.csv
    Merge-CSVsInDirectory `
      $PosChangesToMerge `
      $MergedChanges
    $CollectedChangeCount = @(Import-CSV $MergedChanges).count
    if($CollectedChangeCount -gt 0) {
        Write-Verbose "$CollectedChangeCount position changes to ponder"
        & 'conda.exe' run -p lgcce python `
          -m learned_group_changes `
          -c config-contoso-prod.yaml `
          --users-csv ad-user.csv `
          --groups-csv ad-group.csv `
          --users-groups-csv ad-user-group.csv `
          --position-change-csv $MergedChanges `
          -o $ThisRecommendation
        # didn't we say to stop if there were errors...?
        # ahhhh.... https://github.com/conda/conda/issues/8385
        # "conda run doesn't forward the exit code" in v. 4.7.12
        if($?) {
            if(Test-Path $ThisRecommendation) {
                $new_count = (Import-CSV $ThisRecommendation).count
                if($new_count -gt 0) {
                    Write-Information "There are ${new_count} new group change recommendations due to personnel position changes."
                }
                # we have made our recommendations; don't make them over again next run
                Move-Item -Force $MergedChanges $OldMergedChanges
            }
            Merge-CSVsInDirectory `
              $RecommendationsToMerge `
              $MergedRecommendations
        } else {
            write-warning "The learner did not succeed."
        }
    } else {
        Write-Verbose "No position changes to ponder; doing nothing"
    }
} 3> warnings.txt 4>verboses.txt

if((gc warnings.txt | measure).count -gt 0) {
    Write-Warning "Warnings found! Emailing admins."
    gc warnings.txt | Write-Warning
    Send-MailMessage -SmtpServer mail.contoso.com -To 'lgcadmins@contoso.com' -From 'learned-group-changes@contoso.com' -Subject 'Warnings from learned-group-changes' -Body $(gc warnings.txt | Out-String)
}

if((gc verboses.txt | measure).count -gt 0) {
    Write-Warning "Verboses found! Emailing admins."
    gc verboses.txt | Write-Verbose
    Send-MailMessage -SmtpServer mail.contoso.com -To 'lgcadmins@contoso.com' -From 'learned-group-changes@contoso.com' -Subject 'Verboses from learned-group-changes' -Body $(gc verboses.txt | Out-String)
}
